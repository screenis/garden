## Screen.is Research Garden

This design is based on [Andy's Working Notes](https://notes.andymatuschak.org/About_these_notes), which saves you from having to command-click internal links to open in new tabs, instead loading them in columns. It's running on [Gatsby](https://www.gatsbyjs.com/), in a [theme](https://github.com/mathieudutour/gatsby-digital-garden/) by [Mathieu](https://mathieu.dutour.me/). There's [another very nice](https://github.com/aravindballa/gatsby-theme-andy) Gatsby/Andy's Notes theme, but it doesn't have search.

It's a work-in-progress: typos and inaccuracies the responsibility of [Nicol](https://github.com/vingle). The repo is at https://gitlab.com/screenis/garden/.

Any original content is [CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/). 
