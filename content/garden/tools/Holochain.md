# Holochain

**Holochain**
Somewhat similar, but more open. https://holochain.org/. They once tweeted "Holochain is the solution for Climate Change". "Securing the Holochain chain requires no mining which causes it to have a minimal carbon footprint". Rather than using the entire ecosystem to provide consensus, they use 7 random witnesses. 

"Holochain starts with a fundamentally different perspective from either blockchain or client/server architectures: an acknowledgement that all information has its origin in the subjective experience of  the agent (usually a person) producing it, and **the argument that data separated from its provenance has lost a critical part of its meaning** (Harris-Braun, 2021)." ([Exploring Co-Design Considerations for Embedding Privacy in Holochain Apps](https://dialnet.unirioja.es/servlet/articulo?codigo=8036267): A Value Sensitive Design Perspective, [Paul d’Aoust](https://dialnet.unirioja.es/servlet/autor?codigo=5372969); [Oliver Burmeister](https://dialnet.unirioja.es/servlet/autor?codigo=5098733); [Anisha Fernando](https://dialnet.unirioja.es/servlet/autor?codigo=5096191); [Anwaar Ulhaq](https://dialnet.unirioja.es/servlet/autor?codigo=5372970); [Kirsten Wahlstrom](https://dialnet.unirioja.es/servlet/autor?codigo=5099109)

The architecture explained: https://developer.holochain.org/concepts/2_application_architecture/
- Zomes,
- DNA
- Conductors
- Messagepack https://msgpack.org/
- DHT: A Shared, Distributed Graph Database
- Searching / indexing DHT https://developer.holochain.org/concepts/4_dht/#finding-peers-and-data-in-a-distributed-database
- Links - tripple between base and target entity and a name/word
- Anchors - username, unique ids, uris

An application (https://developer.holochain.org/references/)
The DNA is what lives in Holochain and is written in Rust. 

## Holochain KYC
https://medium.com/h-o-l-o/verification-announcement-for-the-holo-network-c8db9b8eaddc

## More sustainable

On the thermodynamics of Bitcoin, Dogecoin, Ethereum, and Holochain
Posted 21 May 2021

> Public blockchains consume so much electricity because they have a lot of entropy to deal with. In thermodynamics, entropy is noise, waste, meaninglessness. The problem blockchain is trying to solve — dishonest agents trying to corrupt the global ledger — certainly fits that description. 
> Any time you have too much entropy, you’re going to need to spend a lot of energy [moving it somewhere else](https://www.reddit.com/r/explainlikeimfive/comments/mgjfkn/eli5_what_is_negentropy/gstkwly/). **My hot take: Bitcoin is basically converting dishonesty into CO₂.** It’s simple physics.

https://blog.holochain.org/satoshi-nakamoto-and-the-fate-of-our-planet-2/"

![[Pasted image 20210528015821.png]]

### Dev experience
How viable is it so far? https://forum.holochain.org/t/what-happs-have-been-used-in-real-life-so-far/4542/6
"As a personal note about development, I have found the use of both javascript + rust within the Holochain ecosystem challenging, as they’ve not been kept in sync 100% of the time and Holochain struggles with good developer communication about changelogs, semver and breaking changes. For this I’ve been finding it easier to now write unit tests within Rust itself as well, and limit my use of the holochain javascript libraries as much as possible." https://github.com/Connoropolous. https://sprillow.com/. Connor Turland, co-creator of Acorn https://www.youtube.com/watch?v=-9GQN-Px12E

- **[How to ship your hApp](https://holochain-open-dev.github.io/blog/how-to-ship-your-happ/)**

hApps
- Acorn - download: https://github.com/h-be/acorn; walkthru: https://www.youtube.com/watch?v=skJN8vpv6pA&t=845s
- Junto - social network - https://www.youtube.com/watch?v=z4Fb5x1RAmk | https://junto.foundation/ | [Crowdfunding](https://www.indiegogo.com/projects/junto-a-new-breed-of-social-media--2#/)
- CoGove - community governance - https://cogov.tech/
- RedGrid - "intelligent software that supports the adoption of clean energy hardware." energy grids, from household energy monitor / smart home, to property energy mgmt - https://redgrid.io/
- Many more listed [here](https://forum.holochain.org/c/projects/7)

Lightwave - sustainable marketplace, promised - https://forum.holochain.org/t/lightwave-marketplace-for-sustainable-products/3980 - https://lightwave.ch/



### Devs
- [Guillem Cordoba](https://www.youtube.com/channel/UC8UENxhMo_EREr3ST9SNgdQ)

### Founders
co-founders Eric Harris-Braun and Arthur Brock.
https://www.youtube.com/watch?v=2FJL3ibnZlY
related to Holochain is 
- Commons Engine - https://commonsengine.org/
- MetaCurrency - https://metacurrency.org/about/
