      

ssh pi@192.168.0.33

**Maintenance**
sudo apt update - get a list of what needs updating
sudo apt upgrade - upgrades them
sudo apt-get dist-upgrade - upgrades but can delete old packages and do other tidying
NB - apt is ubuntu's alternative to apt-get https://itsfoss.com/apt-vs-apt-get-difference/


**Network mgmt**
sudo nmap -sn 192.168.1.0/24 - search local network for IPs of connected devices

**User mgmt**
psswd - changes password for logged in user
sudo su - changes user to super user
su pi - changes user to 'pi'
sudo -aG usermod sudo {username} - add a user to the sudo group

**Permissions**
-   **chmod +rwx filename** to add permissions.
-   **chmod -rwx directoryname** to remove permissions.
-   **chmod +x filename** to allow executable permissions.
-   **chmod -wx filename** to take out write and executable permissions.

**Raspberry Pi-specific**
raspi-config
 - setting up a server: https://www.tomshardware.com/news/raspberry-pi-web-server,40174.html
 