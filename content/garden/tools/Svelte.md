# Svelte
Not unlike [[Electron]], takes the concept further by compiling JS libraries into only what's needed for an app, ie the browser doesn't do the work, the compiler does.

- site - https://svelte.dev/
- intro blog - https://svelte.dev/blog/svelte-3-rethinking-reactivity
- intro presentation - https://www.youtube.com/watch?v=AdNJ3fydeao&t=5s

