# Jekyll

## Some terms
- Gems - like code packages/modules/plugins
- Gemfile - Like composer.json, the list of the packages used
- Bunder - Package installer 
- Front matter - sets variables for the page at the head.

## Concepts
### Liquid - the template engine
Liquid is made up of:
- Objects, predefined variables as content, appearing inside {{ }}, e.g.  {{ page.title }} shows the page title variable.
- Tags, define logic, and appear like this {% if page.title %}Then{% endif %}
- Filters change the output, separated from an object with a pipe, ie {{ "page.title" | capitalize }} 

### Layouts
Templates that can be used by any page, and that are set in the Front Matter

### Includes
Files stored in the _includes_ folder can be called in templates - ie  a menu or footer, ie {{% include navigation.html %}}

### Data files
Can be YAML, JSON or CSV aand live in the _data_ directory

```
<ul>
  {% for post in site.posts %}
    <li>
      <h2><a href="{{ post.url }}">{{ post.title }}</a></h2>
      {{ post.excerpt }}
    </li>
  {% endfor %}
</ul>
```

and with filter

```
---
layout: default
---
<h1>{{ page.name }}</h1>
<h2>{{ page.position }}</h2>

{{ content }}

<h2>Posts</h2>
<ul>
  {% assign filtered_posts = site.posts | where: 'author', page.short_name %}
  {% for post in filtered_posts %}
    <li><a href="{{ post.url }}">{{ post.title }}</a></li>
  {% endfor %}
</ul>
```

## Deployment [ref](https://jekyllrb.com/docs/step-by-step/10-deployment/)
- configure your Gemfile. add plugins like jekyll-seo-tag or -feed