# Activity Pub

https://www.w3.org/TR/activitypub/

The ActivityPub protocol is a decentralized social networking protocol based upon the [ActivityStreams](https://www.w3.org/TR/activitypub/#bib-ActivityStreams) 2.0 data format. 

It provides a client to server API for creating, updating and deleting content, as well as a federated server to server API for delivering notifications and content. It is used by [[Peer Tube]].

### List of Fediverse apps…
from https://git.feneas.org/feneas/fediverse/-/wikis/watchlist-for-activitypub-apps
