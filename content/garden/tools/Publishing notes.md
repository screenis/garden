
# Publishing Notes

After recording [[Node Notes]], they need publishing.

**The Neutron philosophy** - https://neuron.zettel.page/philosophy#fn2
 - keep local copies
 - decouple from a single editor
 - static generator for web publishing
 - version control & history with Git

with Obsidian - a [paid publishing model](https://obsidian.md/publish), for $8/month.
outside of obsidian, with some problems (ie no [[]] links)
 - https://forum.obsidian.md/t/obsidian-github-pages-for-digital-gardeners/2622/5
 - https://forum.obsidian.md/t/understanding-digital-garden-creation-with-obsidian/2725/9.

Otherwise you need to use [[Static Site Generators]].

- Using with **VuePress** - https://github.com/spencerwooo/foam-wiki
- Using with **[[Jekyll]]** 
	- https://notenotelink.netlify.app/notes/obsidian-integration 
	- https://github.com/raghuveerdotnet/simply-jekyll 
	- https://forum.obsidian.md/t/notenote-link-publish-your-obsidian-notes-with-jekyll-for-free/7951 
	- https://dev.to/yordiverkroost/publish-your-obsidian-vault-to-your-digital-garden-5bf8
	- https://github.com/Maxence-L/notenote.link
	- https://forum.obsidian.md/t/jekyll-integration/8544
	- https://talk.jekyllrb.com/t/zettelkasten-in-jekyll/5218
- Using with **HTML** - https://github.com/srid/neuron/ 
- Using with **Gatsby** - https://www.npmjs.com/package/gatsby-theme-networked-thought?activeTab=readme
- Using with **Hugo** - https://forum.obsidian.md/t/how-to-deal-with-markdown-flavor-when-publishing-from-obsidian/4842
- Using with **Lettersmith ** - https://github.com/kmcgillivray/obsidian-lettersmith
