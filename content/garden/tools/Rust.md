# Rust
Good intro: https://github.com/readme/featured/rust-programming

- In 2021, Mozilla transferred stewardship of Rust to the Rust Foundation, a coalition founded by Amazon Web Services, Facebook, Google, Huawei, Microsoft, and Mozilla
- It's known as a 'memory safe' language, which means it manages memory automatically, reducing the risk of programmers adding bugs ("Around 70 percent of all security issues fixed between 2004 and 2018 in Microsoft products were memory safety issues")
- Rust was ranked the “most loved” programming language in [Stack Overflow’s 2021 Developer Survey](https://insights.stackoverflow.com/survey/2021) for the sixth year running.
- "many programmers complain that Rust has a steep learning curve compared to other modern languages"