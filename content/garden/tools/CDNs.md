# Content Delivery Networks
Content Delivery Networks often don't publish their pricing. The biggest are:
 - Akami
 - Limelight
 - [Havision](https://www.haivision.com/) (inc Teltoo) - they are behind the open source low-latency SRT streaming protocol (https://www.haivision.com/blog/all/excited-srt-video-streaming-protocol-technical-overview/) - they [integrate](https://www.haivision.com/products/video-services/hvc/) with Akami. 
 - Microsoft Azure - https://azure.microsoft.com/en-us/pricing/details/media-services/
 - GCoreLabs - https://gcorelabs.com/pricing/ - good price
 - CDN77 - https://www.cdn77.com/pricing/monthly-plans
 - Amazon Cloudfront
 - Key CDN - cheap, Swiss https://www.keycdn.com/
 - Cloudflare - they move data around their global CDN to find points of downtime, so it's cheaper (which might mean data travels further) https://blog.cloudflare.com/how-cloudflare-streams/

Some pricing:

**Fastly:**
North America & Europe – First 10tb - $0.12/gb. Next 10GB - $0.08
Asia, South America, Oceana – $0.19 - $0.14
Brazil, India, Africe – $0.28 - $0.24
plus $0.075 - 0.0160 per 10,000 requests
https://www.fastly.com/pricing

**Worked example**
"500kbps stream quality (medium. High is recommended for sports events)  
1000 simultaneous viewers  
2 hours length
You need 450GB. You pay $0.2 per GB, so your total cost is $90" = $0.09/per viewer or $0.045/hour. So $0.1/gb = $0.0225. Or for HD 4mbps = $0.18
https://www.quora.com/How-expensive-is-it-on-average-to-stream-online-video

700mb SD / 4Gb HD - for a 2 hour film. So Fastly = $0.16/hour

**Microsoft Azure**
$0.087 – $0.05 per GB

**GCore Labs**
€0.0025/GB - 1tb free - limited locations
Pro 5tb plan - €0.02 5tb pre-bought for €100
Pro 10tb - €0.015 10,000tb pre-bought for €150 - €0.06/4gb hd feature

**Amazon Cloudfront**
$0.085/GB for the first 10 TB of data

**Key CDN**
North America & Europe - First 10 TB are $0.04/GB
Asia and Oceania - First 10 TB are $0.09/GB

**Stackpath**
$0.02/gb - https://www.stackpath.com/

### How much data?
https://www.dataplugs.com/en/how-much-bandwidth-is-needed-for-video-streaming/
4k: 13–34 Mbps
1080p: 3–6 Mbps
720p: 1.5 – 4 Mbps
per person.
" 6 percent of viewers leave per second of buffering"

## Or using PTP & the newer streaming protocols..

Let other streamers upload some of the data.. can reduce 50-70% of the streaming costs.
https://teltoo.com/the-benefits-of-cdn-offload/

### SRT
"Secure Reliable Transport (SRT) is a rising star in the streaming frontier. It delivers high quality video and audio with low latency over the unreliable public Internet. You can actually control the amount of latency and eliminate issues like jitter due to packet loss over poor networks."
https://www.epiphan.com/blog/why-srt-hls-mpegdash-streaming/

### HTTP Live Streaming (HLS)
"HTTP Live Streaming (HLS) is an adaptive, HTTP-based streaming protocol that sends video and audio content over the network in small, TCP-based media segments that get reassembled at the streaming destination. The cost to deploy HLS is low because it uses existing TCP-based network technology, which is attractive for CDNs looking to replace old (and expensive) RTMP media servers. But because HLS uses TCP, Quality of Experience (QoE) is favored over low latency and lag times can be high (as in seconds instead of milliseconds)."
https://www.epiphan.com/blog/why-srt-hls-mpegdash-streaming/

### MPEG-DASH (Dynamic Adaptive Streaming over HTTP)
"MPEG-DASH is an open standard, adaptive HTTP-based streaming protocol that sends video and audio content over the network in small, TCP-based media segments that get reassembled at the streaming destination. The International Standards Organization (ISO) and the team at MPEG designed MPEG-DASH to be codec and resolution agnostic, which means MPEG-DASH can stream video (and audio) of any format (H.264, H.265, etc.) and supports resolutions up to 4K. Otherwise, MPEG-DASH functions much the same as HLS."
https://www.epiphan.com/blog/why-srt-hls-mpegdash-streaming/