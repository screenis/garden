# Byzantine Fault Tolerent

### The Byzantine Problem
from: https://www.hcltech.com/blogs/byzantine-fault-tolerance-bft-and-its-significance-blockchain-world

The Byzantine problem is defined in a paper published by Leslie Lamport, Robert Shotak, and Marshall Pease in 1982. The paper was named The Byzantine Generals Problem. It stated an allegory for the problems of achieving consensus in a decentralized system.

The allegory goes like this: “Before battle, the Byzantine generals, commanding different army battalions, try to decide whether they should attack or retreat. This has to achieve by passing the messages via messengers between them and coming to an agreement. However, there is a problem. There is a possibility that some of the generals and/or some messengers may be traitors to the cause. These traitorous generals and messengers may alter the message and pass the malicious response sabotaging the plans of the loyal generals. So, the loyal generals need to find a way to reach a consensus having the above information in hand.

For blockchain technology/distributed ledger technology, the above allegory can be translated to the environment where the generals can be replaced by nodes, and messengers by the connections or messaging protocols.

With time, there were attempts to provide a solution on this distributed network/blockchain-centric problem. At present, we have many solutions for distributed networks which provide the partial answer to this issue if not full. In blockchain, various consensus mechanisms (protocols to reach an agreement in a distributed system) are designed which inherently deals with the Byzantine issue.