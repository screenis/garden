# Tools

## Research Gathering

-  [[Node Notes]] for storing research in a network rather than a list. That's what powers this garden
-  [[Publishing notes]] 

## Web technologies
 - [[Webmentions]]
 - [[Activity Pub]]
 - [[Electron]]
 - [[Self-sovereign Identity]]
 - [[Static Site Generators]], including [[Jekyll]]

## Payments
- [[Open Payments]]
- [[Payment Protocols]]
- [[Interledger]]
- [[ISO 20022]]
- [[Web Payments]]

## Distributed databases
- [[Distributed Database (DDBMS)]]
- [[Big Chain DB]]
- [[SoLiD]]

## Network
- [[CDNs]]
- [[Netflix]]

## Optimisation
- [[Svelte]]
- [[Tiny css]]

## Blockchain concepts
 - [[Byzantine Fault Tolerent]]
 - [[Holochain]]

## Decoupled Architecture

- "Jamstack" = an architecutre based on the principle of pre-rendering (as with [[Static Site Generators]] and decoupling ("creating a clean separation between systems or services"). 
- Microservices = somewhat siimilar to Jamstack, all of [[Netflix]] is built as a mesh of microservices, see lecture: https://www.youtube.com/watch?v=CZ3wIuvmHeM.

## Tech notes
- [[The Shell]]
- [[Composer]]