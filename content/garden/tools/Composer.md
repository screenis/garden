# Composer

## Basic Composer Commands

- `composer require drupal/uikit` - install drupal/uikit
- `composer require 'drupal/<modulename>:<version>'` require specific version (ie `require 'drupal/ctools:3.0.0-alpha26'`)
- `composer subtheme my_theme` - create subtheme (from within the directory)

