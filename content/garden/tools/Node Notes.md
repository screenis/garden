---
title: "notes"
---

# Node-based notetaking & research…

- Zettelkasten - what is it: https://zettelkasten.de/posts/overview/
- How my second brain made me happier https://www.datatheism.com/organised-magic/amp/?__twitter_impression=true
- Alternatives to Roam: https://www.reddit.com/r/selfhosted/comments/f0z6yd/open_source_alternatives_to_roam_research/
	- [Trillium](https://github.com/zadam/trilium). Local (Windows/Linux) or server based (Kubernetes/Docker/Cloudron/Node). + [Looks cool](https://github.com/zadam/trilium/wiki/Screenshot-tour) – but not necessarily easy to setup on my machine/server.
	- [DokuRoam](https://github.com/andjar/dokuroam). Runs on [DocuWiki](https://www.dokuwiki.org/dokuwiki) which needs a LAMP setup. + wiki collaboration & ACL. - files stored as .txt not .md so not compatible with Obsidian/etc. (there is a [Markdown plugin](https://www.dokuwiki.org/plugin:markdownextra) but it's not been updated in years).
	- [TiddlyWiki]() has three options. It can be installed in lots of ways, including on [Gitlab](https://gitlab.com/danielo515/tw5-auto-publish2gitlab-pages).
		- [TiddlyRoam](https://joekroese.github.io/tiddlyroam/) - bidirectional links and graph maps. Example: https://citizense.org/wiki
		- [IdeaStew](https://giffmex.org/gifts/ideastew.html) recommended deprecated by its author who says instead use:
		- [Stroll](https://giffmex.org/stroll/stroll.html)
	- [Obsidium](https://obsidian.md/)