# Tiny css
What is the smallest CSS files that can provide a grid, be responsive and handle cleaner presentation of base HTML elements?

As well as reducing the carbon footprint of pages - tiny CSS libraries improve speed (Amazon apparently found each 100ms delay cost them over a billion - unreferenced claim from [here](http://mincss.com/))

## Libraries

### Picnic 
By [Francisco Presencia](https://github.com/FranciscoP) - under 10kb, includes js-free CSS transitions for multi-step forms, for instance. Builds on SASS. Includes TABs based on [Felipe Fialho](https://css-components.felipefialho.com/#component-tab)'s work. https://picnicss.com/

### Miligram
2kb gzipped. Nice and elegant - handles all the basics, with two button defaults (solid and outlined). https://milligram.io

### Pure
https://purecss.io/

### Skeleton
400 lines - covers the basics, built around Raleway so has a minimal clean look. Uses [Normalize.css](https://github.com/necolas/normalize.css/) a widely used reset library.
http://getskeleton.com

### Min
http://mincss.com/

## CSS icons

- **CSS icons** - https://css.gg/app

## CSS-only no-JS libraries
- **CSS Components** - Includes modal, accordian, dropdown, tab and tooltip: 
https://css-components.felipefialho.com/