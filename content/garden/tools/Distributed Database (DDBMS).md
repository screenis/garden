# Distributed Database (DDBMS)
When the storage devices for data are spread across a network. "DDBMS use consensus mechanisms to ensure fault-tolerant communications, and provide concurrency control through locking and/or time-stamping mechanisms." ([ref](https://medium.com/@sbmeunier/blockchain-technology-a-very-special-kind-of-distributed-database-e63d00781118)).

![](https://miro.medium.com/max/945/0*poJcCxM33T7Ae9tw.jpg)

DDBMS include:

- **Peer Network Node Data Stores**, using peer-to-peer methods like BitTorrent, NNTP, Freenet, Mnet.
- **Distributed SQL data warehouses** allow for parrallel processing on massive datasets.
- [**Hadoop**](http://hadoop.apache.org/), works "on clusters of commodity hardware. It provides massive storage for any kind of data, enormous processing power and the ability to handle virtually limitless concurrent tasks."
-  [**NoSQL**](http://en.wikipedia.org/wiki/NoSQL) databases are horizontally scalable, non-relational DDBMS, and include: MarkLogic, MongoDB, Datastax, Apache [Cassandra](https://cassandra.apache.org/), Redis, Riak, Google BigTable and CouchDB.
-  [**NewSQL**](http://www.datavail.com/blog/what-is-newsql/), combining qualities of NoSQL with traditional relational (RDBMS) dbses, including Google Spanner, Clustrix, VoltDB, MemSQL, Pivotal’s GemFire XD, NuoDB and Trafodion.
-  **Distributed Ledgers (DL)**, e.g. Bitcoin, which don't have any kind of central control (unlike NewSQL, NoSQL, etc).
-  **[R3 Corda](http://static1.squarespace.com/static/55f73743e4b051cfcc0b02cf/t/57bda2fdebbd1acc9c0309b2/1472045822585/corda-introductory-whitepaper-final.pdf)** is a distributed ledger designed to operate in regulated environments with a limited number of known participants where consensus about a transaction is basically reduced to its validation by the two contracting parties.
-   **Hashgraph** is a distributed ledger using a *gossip protocol*, to share information across a network. It's faster, more decentralised, and less energy intensive the proof-of-work and proof-of-stake ledgers. BUT fully proprietary, deployment of apps depends on permission from the developers and owners Swirlds.com. https://www.swirlds.com/resources/
- **[[Holochain]]** - like Hashgraph uses Gossip, but isn't closed. 
- **[[Big Chain DB]]**