# Photo DNA	
Microsoft's [Photo DNA](https://www.microsoft.com/en-us/photodna) is a media hashing system used for photos and videos. Apparently:

"an image can be resized, cropped or colours changed and our hashes will still find a match to the original image."

In 2009, Microsoft partnered with Dartmouth College to develop PhotoDNA, a technology that aids in finding and removing known images of child exploitation. PhotoDNA Cloud Service is free for non-profits & law enforcement.

It is used by over 200 organisations worldwide, including the Internet Watch Foundation's [Hash List](https://www.iwf.org.uk/our-services/hash-list) in association with ChildLine, and the National Center for Missing & Exploited Children (NCMEC).

## Video

It was designed for photos but they developed a video version with IWF: https://news.microsoft.com/on-the-issues/2018/09/12/how-photodna-for-video-is-being-used-to-fight-online-child-exploitation/.

It works by taking key frames from the video, and generating hashes from them. Apparently:

“When people embed illegal videos in other videos or try to hide them in other ways, PhotoDNA for Video can still find it. It only takes a hash from a single frame to create a match.”

![Graphic showing how DNA for Video creates hashes from video frames and compares with known images](https://3er1viui9wo30pkxh1v2nh4w-wpengine.netdna-ssl.com/wp-content/uploads/prod/sites/358/2018/08/Photo-DNA-for-VIdeo-how-to.jpg)