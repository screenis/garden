# Static Site Generators

## Gitlab's guide:
- part 1, background: https://about.gitlab.com/blog/2016/06/03/ssg-overview-gitlab-pages-part-1-dynamic-x-static/
- part 2: SSGs: https://about.gitlab.com/blog/2016/06/10/ssg-overview-gitlab-pages-part-2/
- part 3: using Gitlab: https://about.gitlab.com/blog/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/

- Using with **Joomla** - https://www.youtube.com/watch?v=AHpfhnsbYXY&t=69s
- Using with **Tiddly** - https://nesslabs.com/tiddlywiki-static-website-generator

## The Generators

Seems to be a continuum of complexity with Gatsby at one end, with the nicest themes but most complex file output; and Eleventy at the other, with few themes but simpler/cleaner output. Jekyll sits in the middle somewhat as a Goldilocks SSG.

### Gatsby - React/Node - 

https://www.gatsbyjs.com

++ Crazily fast. Handles data sources via React. Components. Lots of plugins. Andy theme.
- Creates so many files. HTML/DOM output is unreadable. It doesn't want you to use the static files; they're just there. A three page static site is 305mb, and 55,604 files. How is that secure? Even excluding the Node modules, there's 49 files, 2.7mb for a 3 page site.
++ But andy's & Mathieu's themes are so nice.
- But the speed looks like it's because each page is turned into json and then dynamically generated server side?

## [[Jekyll]] -Ruby 

https://jekyllrb.com 
- long time and lot of effort setting up ruby. Final result's underwhelming, and adding a new page doesn't generate anything.  - 26 items, but didn't include Ruby.
but then: https://simply-jekyll.netlify.app/posts/exploring-the-features-of-simply-jekyll
and https://digital-garden-jekyll-template.netlify.app/cats
and https://maximevaillancourt.com/blog/setting-up-your-own-digital-garden-with-jekyll
and https://refinedmind.co/obsidian-jekyll-workflow

NEXT NIGHT>> it never worked because I was running off the wrong Ruby, and had to install that. 
Removing the filename: https://stackoverflow.com/questions/27099427/jekyll-filename-without-date/27101060
So it works. And it works nice. 
https://stackify.com/install-ruby-on-your-mac-everything-you-need-to-get-going/
https://refinedmind.co/obsidian-jekyll-workflow

## Hugo - Go -
https://gohugo.io/ - 
this is easy to install, quick to generate, load, navigate, intuitive, not too many files - 149 items / 12mb, not including Go. 134 items are for the theme Ananke. Good seo defaults.
Nice looking themes: https://getdoks.org/docs/prologue/introduction/
https://adityatelange.github.io/hugo-PaperMod/
https://themes.gohugo.io//theme/hugo-theme-diary/
https://themes.gohugo.io//theme/hugo-theme-cactus
backlinks - https://hugokasten.netlify.app/features.html

### 11ty - JS/Node
https://github.com/11ty/eleventy / https://www.11ty.dev/ / 
Nice but too simple out the box, demands time to learn how to make something nicer. Smaller template selection than the others. But it seems a good bridge because it can read all the other template engines; and isn't particular about folder names/structure, so a Jekyll/Gatsby site could be ported to 11ty later.

### Next.js
205mb + 12,387 files, including React/node. But very nice tutorials and somehow felt more mature than Gatsby. Fewer Digital Garden themes, however.

### Others
Hexo - Node - https://hexo.io/docs/setup
Scully - JS/Angular - https://scully.io/
Pelican - Python - https://blog.getpelican.com/

## Comparing their speed
### Build time
https://css-tricks.com/comparing-static-site-generator-build-times/
TLDR: advanced SSGs like Gatsby & Next are slow; simpler like Jekyll, Eleventy and Hugo are quicker – Hugo the quickest; gatsby the slowest. 

