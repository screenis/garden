# Open Payments

Open Payments is a Protocol to setup payments between entities on the Web based on OAuth. It is a REST API building on top of [[Interledger]], with 4 resource definitions:

-   [accounts](https://docs.openpayments.dev/accounts)
-   [invoices](https://docs.openpayments.dev/invoices)
-   [mandates](https://docs.openpayments.dev/mandates)
-   [charges](https://docs.openpayments.dev/charges)

The API also defines **alternative resource representations** for invoices and accounts that contain [payment details](https://docs.openpayments.dev/payments) for making payments against those invoices or into those accounts.

The URL of the **accounts** resource is [discovered](https://docs.openpayments.dev/discovery) by the client and all other API endpoints are then relative to that URL. So for a [[Payment pointers]] of $ua.com/mary-pickford, the invoice API endpoint would be https://ua.com/mary-pickford/invoices

**Sending**
"To send a specific amount of money the sender creates an invoice via the [invoice API](https://docs.openpayments.dev/invoices) at the receiver and gets the payment details for that invoice as defined in the [payment details API](https://docs.openpayments.dev/payments)."

**Receiving**
"In order to receive money over Open Payments, an invoice resource is created at the receiving wallet and the URL of the invoice is presented to the sender."

**Mandates**
"By using the [mandates API](https://docs.openpayments.dev/mandates), 3rd parties can request authorization from an account owner to initiate payments from their account (once-off, on a recurring basis, now, or at future date)."

**Authorisation**
"All API endpoints can be protected as determined by the wallet and account owner based on their requirements."
https://docs.openpayments.dev/auth

Per the latest [specification](https://tools.ietf.org/html/draft-ietf-oauth-rar-01#section-2.1) the object MUST contain an array of objects, each with a `type`, `locations` and `actions` property.

-   `type` is one of the resource types defined in Open Payments
-   `locations` is any array of URLs that provide the location of the resources
-   `actions` is an array of actions that the client is requesting permission to perform on the resource.

In general, the `actions` defined are `create`, `read`, `update` and `delete`. In addition to this, clients can be given `read_own`, `update_own` and `delete_own` permissions which allows them to read, update and delete resources that they have created.

**Currency**
"An invoice MUST always inherit the currency of the underlying account it is created against."

**Mandate Currency**[#](https://docs.openpayments.dev/currency#mandate-currency "Direct link to heading")
"Mandates are created with an asset code and scale chosen by the client. This MAY be different to the currency of the underlying account the mandate is created against.
"When a wallet requests an account owner to authorize a mandate the wallet MUST display the terms of the mandate in both the currency of the mandate AND the currency of the underlying account."