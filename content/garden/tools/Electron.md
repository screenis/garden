# Electron JS
Lets you build cross-platform apps with JS, HTML and CSS, with an API to interact with menus, windows, updaters, and installers.

This electron app: https://github.com/electron/electron-api-demos - shows the different features available.

### Electron Python
From [this article](https://medium.com/@abulka/electron-python-4e8c807bfa5e), came this solution - https://github.com/fyears/electron-python-example - described here: https://www.fyears.org/2017/02/electron-as-gui-of-python-apps-updated.html.

Flutter is somewhat similar, but adds iOS and Android support: https://flutter.dev/. It also works with [[Python]]:
 - https://smazee.com/blog/how-to-run-the-python-code-in-the-flutter-app
 - https://www.asapdevelopers.com/flutter-login-app-with-python-backend/


