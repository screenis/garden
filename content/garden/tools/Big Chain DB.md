# Big Chain DB
**"[White Paper](http://www.bigchaindb.com/whitepaper/bigchaindb-whitepaper.pdf)**. 

"With high throughput, low latency, powerful query functionality, decentralized control, immutable data storage and built-in asset support, BigchainDB is like a database with blockchain characteristics." 

Or in other words, it's MongoDB connected with other MongoDBs via a blockchain.

https://www.bigchaindb.com/ 

## Key concepts
https://www.bigchaindb.com/developers/guide/key-concepts-of-bigchaindb/

**Data Structure**
"Traditional SQL databases structure data in tables. NoSQL databases use other formats to structure data such as JSON and key-values, as well as tables. At BigchainDB, we structure data as assets. These assets can be registered on BigchainDB in `CREATE` transactions and transferred to other users in `TRANSFER` transactions.

**Architecture**
BigchainDB 2.0 uses Tendermint for all networking and consensus. Eachnode has its own local MongoDB databas, and all communication betweennodes is done using Tendermint protocols. Tendermint is [[Byzantine Fault Tolerent]] (BFT), ie it can cope with bad/malicious actors.

## Becoming Oceon Protocol
They pivotted to a data selling marketplace: https://oceanprotocol.com

From the question how can you securely “own” data, and sell it while avoiding “data escapes”, with Toyota Research Institute, they [built](https://techcrunch.com/2017/05/22/toyota-pushes-into-blockchain-tech-to-enable-the-next-generation-of-cars) a prototype data marketplace that used BigchainDB.

This initial foray into decentralized data marketplaces grew into a much larger project: [Ocean Protocol](https://www.bigchaindb.com/about/www.oceanprotocol.com). The team at BigchainDB GmbH (the company) is now focused on Ocean Protocol. [IPDB Foundation](https://www.bigchaindb.com/about/ipdb.io) has [assumed governance](https://medium.com/ipdb-blog/ipdb-foundation-assumes-governance-of-bigchaindb-software-and-testnet-51235322e14c) of the BigchainDB software and networks.

Ocean Protocol "is governed by a Singapore based non-profit foundation, whose mandate is to ensure open access to the protocol and platform, provide data governance, encourage the network ecosystem growth, and take measures to ensure that the platform becomes ever more decentralized with time."