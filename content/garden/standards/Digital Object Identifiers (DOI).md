# Digital Object Identifiers (DOI)

https://www.doi.org/

An identifier used in [[Digital Object Architecture (DOA)]].

-  Launched in 1998 with first applications in 2000
-  Currently used by over 5,000 assigners
-  Approximately 257 million DOI names assigned to date
-  Over 150,000 DOI name prefixes within the DOI System

Full information in the handbook - https://www.doi.org/hb.html