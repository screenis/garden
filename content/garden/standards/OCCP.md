# Open Content Certification Protocol (OCCP)

https://github.com/licium/occp/blob/master/docs/main.md

ISCC opens up the challenge of multiple registrations of the same piece of content by multiple parties. How is trust established when anyone can claim they wrote the Beatles back-catologue? 

OCCP is a method to register certification entities. These aren't necessarily the same entity as the content author, or owner - but they are permissioned with registering metadata around a specific ISCC code; for instance a record label, a production company, a photography club or website.

OCCP doesn't confer any information on an ISCC other than it was registerd by a certified entity, which at a minimum is a legal entity, as recognised with a Legal Entity Identifier ([[LEI]]), which can be generated for a fee.

![Open Content Certification Overview](https://github.com/licium/occp/raw/master/docs/images/occp-image5.png)