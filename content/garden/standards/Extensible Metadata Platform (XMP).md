# Extensible Metadata Platform (XMP)

An ISO standard, BSD licensed, created by Adobe in April 2001, for the creation, processing and interchange of standardized and custom metadata for digital documents and data sets. It is used in Adobe's [[Content Authenticity Initative]].

XMP standardizes a data model of name/value pairs, typically in an RDF/XML serialization format with core properties for the definition and processing of extensible metadata. This might be embedded in a JPEG or PDF, or may sit alongside the media file as an XML file.

Most commonly it implements Dublin Core metadata, but is extensible and can include binary data such as thumbnails, if encoded in an XML-friendly format like Base64. XMP can both define the entire document and a subset of it.

### Supported formats for embedding
 PDF, JPEG, JPEG 2000, JPEG XR, GIF, PNG, WebP, HTML, TIFF, Adobe Illustrator, PSD, MP3, MP4, Audio Video Interleave, WAV, RF64, Audio Interchange File Format, PostScript, Encapsulated PostScript,
 
 https://www.adobe.com/products/xmp.html