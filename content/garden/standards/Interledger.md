# Interledger
Interledger is a network of computers that enables the sending of value across independent payment networks. Similar to how the internet routes packets information, Interledger routes packets of value. 

Interledger Protocol is not tied to a single company, blockchain, or currency and has multi-hop routing between multiple currencies and wallets.

The Interledger Protocol Suite is comprised of four layers: 
- Application, 
- Transport, 
- Interledger, 
- and Link protocols. 
To learn more, see the Interledger [Architecture Overview](https://interledger.org/rfcs/0001-interledger-architecture/) .

Computers on the Interledger network are called _nodes_. Nodes can take one or more of the following roles:
-   Sender – Initiates a value transfer.
-   Router – Applies currency exchange and forwards packets of value. This is an intermediary node between the sender and the receiver.
-   Receiver – Receives the value.

![[Pasted image 20210617021908.png]]
![[Pasted image 20210617021920.png]]

There is a W3C unapproved draft based on it: https://w3c.github.io/webpayments/proposals/interledger/.

Original White Paper: https://interledger.org/interledger.pdf