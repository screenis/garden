# IMZ Metadata Standard
"International network of the audiovisual community".

A metadata standard associating with ISCC developed by [IMZ](https://imz.at). It includes:
 - Catalogue data - details of the production, credits
 - Marketing data - awards, quoetes, wins
 - Rights data - rights management info

Allows for some data to be kept private and some made public. A [video introduction](https://media.imz.at/innovation/film/imz-academy-tutorial-setting-imz-standards-on-metadata-13653640/).

IMZ is building an integrated [AV standards system](https://standards.imz.at/) featuring:
 - the IMZ metadata standard;
 - unique [[ISCC]] film identifier;
 - European License Ledger - registers licenses for identifiers;
 - Content.Agent - digital marketplace for license trading;