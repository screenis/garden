# Webmentions

**[Webmention](https://www.w3.org/TR/webmention/)** is a web standard for mentions and conversations across the web, a powerful building block that is used for a growing [federated](https://indieweb.org/federated "federated") network of [comments](https://indieweb.org/comment "comment"), [likes](https://indieweb.org/like "like"), [reposts](https://indieweb.org/repost "repost"), and other rich interactions across the decentralized social web.

> ”… an @ mention that works across websites; so that you don't feel immovable from Twitter or Fb.” — [Rony Ngala](https://twitter.com/rngala/status/852354426983591937)

When you link to a website, you can send it a Webmention to notify it. If it supports Webmentions, then that website may display your post as a [comment](https://indieweb.org/comment "comment"), [like](https://indieweb.org/like "like"), or other [response](https://indieweb.org/response "response"), and presto, you’re having a conversation from one site to another!

https://indieweb.org/Webmention