# Coalition for Content Provenance and Authenticity (C2PA)

https://c2pa.org/

C2PA is an 'open technical standard providing publishers, creators, and consumers the ability **to trace the origin** of different types of media" with backers including Adobe, Twitter, Microsoft and the BBC.

"C2PA unifies the efforts of the Adobe-led [[Content Authenticity Initative]] which focuses on systems to provide context and history for digital media, and [Project Origin](https://www.originproject.info/), a Microsoft- and BBC-led initiative that tackles disinformation in the digital news ecosystem.

[[CLink]] is a member.