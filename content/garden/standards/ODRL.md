# Open Digital Rights Language (ODRL)
https://www.w3.org/TR/odrl-model/
Editors: [Renato Iannella](https://au.linkedin.com/in/riannella), [Monegraph](http://www.monegraph.com/), [r@iannel.la](mailto:r@iannel.la) – Monegraph is 'blockchain for media rights', used for NFTs
[Serena Villata](https://www.linkedin.com/in/serenavillata), [INRIA](http://www.inria.fr/en/), [serena.villata@inria.fr](mailto:serena.villata@inria.fr) – French Institute for Research in Digital Science and Technology.

![ODRL Information Model](https://www.w3.org/TR/odrl-model/00Model.png)

## Key terminology:
- **Policy**, a group of rules, with a UID
	- Policy has subclasses; **Set**, for generic Rules; **Offer**, offer of some Rules from Parties; **Agreement**, the granting of Rules from assigner to assignee. 
- **Asset / AssetCollection** - as it sounds, it can be "any form of identifiable resource, such as data/information, content/media, applications, services, or physical artefacts". It needs a UID in IRI standard (https://tools.ietf.org/html/rfc3987)
- **Rule**, a single concept made of permissions, prohibitions & duties
- **Actions**, an operation on an asset (e.g. copy, share)
- **Permissions**, prohibitions & duties define **actions** around assets
- **Constraints** convert Actions around a Rule into a boolean expression
- **Party / Party Collection** - an entity/entities that undertakes a role on a rule, with a UID

ODRL policies must use only terms in 
- **ODRL Core Vocabulary** - set of terms represented in the model
- **ODRL Profile** - sector specific vocabulary to extend ODRL Core Vocabulary with new terms
- The **ODRL Common Vocabulary** - is set of generic terms that may be re-used by Profiles.

For implementation, there's:
- **ODRL validator** - system to check ODRL expressions and validates them
- **ODRL evaluator** - determins if rules have met their action consequences
- **ODRL Core Vocabulary** - set of terms represented in the model
- **ODRL Common Vocabulary** - set of generic terms that may be re-used by Profiles.
- **ODRL Profile** - sector specific vocabulary to extend ODRL Core Vocabulary with new terms

## Policies
- A policy has a uid property value to identify it.
- It must have a rule. 
- For example, a **Set** policy allowing you to access a movie. Sets are the default type of Policy and don't need much more.

```
{
    "@context": "http://www.w3.org/ns/odrl.jsonld",
    "@type": "Set",
    "uid": "http://example.com/policy:1010",
    "permission": \[{
        "target": "http://example.com/asset:9898.movie",
        "action": "use"
    }\]
}
```

- An offer policy, needs an assigner. It only is true if someone takes it up.

```
{
    "@context": "http://www.w3.org/ns/odrl.jsonld",
    "@type": "Offer",
    "uid": "http://example.com/policy:1011",
    "profile": "http://example.com/odrl:profile:01",
    "permission": \[{
        "target": "http://example.com/asset:9898.movie",
        "assigner": "http://example.com/party:org:abc",
        "action": "play"
    }\]
}
```

- An agreement policy needs an assigner and assignee. 

```
{
    "@context": "http://www.w3.org/ns/odrl.jsonld",
    "@type": "Agreement",
    "uid": "http://example.com/policy:1012",
    "profile": "http://example.com/odrl:profile:01",
    "permission": \[{
        "target": "http://example.com/asset:9898.movie",
        "assigner": "http://example.com/party:org:abc",
        "assignee": "http://example.com/party:person:billie",
        "action": "play"
    }\]
}
```

– And it can include other metadata - such as the agreement below between two entities to use an asset

```
{
    "@context": \[
        "http://www.w3.org/ns/odrl.jsonld",
        { "vcard": "http://www.w3.org/2006/vcard/ns#" }
    \],
    "@type": "Agreement",
    "uid": "http://example.com/policy:777",
    "profile": "http://example.com/odrl:profile:05",
    "permission": \[{
        "target": "http://example.com/looking-glass.ebook",
        "assigner": {
            "@type": \[ "Party", "vcard:Organization" \],
            "uid":  "http://example.com/org/sony-books",
            "vcard:fn": "Sony Books LCC",
            "vcard:hasEmail": "sony-contact@example.com" },
        "assignee": {
            "@type": \[ "PartyCollection", "vcard:Group" \],
            "uid":  "http://example.com/team/A",
            "vcard:fn": "Team A",
            "vcard:hasEmail": "teamA@example.com"},
        "action": "use"
    }\]
}
````

## Constraints
Rules can be asociated with parties, assets and actions, and are defined by Constraints. 'The Constraint and Logical Constraint classes form semantic and conditional relationships with the Party Collection, Asset Collection, Action, and Rule classes."

### A constraint class
Is made up up of the **semantics** or terms of the contstraint (left) and the **literal values** for the people/assets/rules (right). It must have a:
 - leftOperand
 - an Operator (<≤=≥>)
 - either a rightOperand/list of them or a rightOperandReference (UIDs)
You could also add info on data type, status or a unit value.
	
In the constraint below, the leftOperand specifies the constraint is time-sensitive, and the right operand set that time before which the permission can be used:

```
{
    "@context": "http://www.w3.org/ns/odrl.jsonld",
    "@type": "Offer",
    "uid": "http://example.com/policy:6163",
    "profile": "http://example.com/odrl:profile:10",
    "permission": \[{
       "target": "http://example.com/document:1234",
       "assigner": "http://example.com/org:616",
       "action": "distribute",
       "constraint": \[{
           "leftOperand": "dateTime",
           "operator": "lt",
           "rightOperand":  { "@value": "2018-01-01", "@type": "xsd:date" }
       }\]
   }\]
}
```

### Logical constraint class
Once multiple constraints exist within the same logical constraint, this compares two or more existing constraints with one logical operator,and will either be satisified or not. The logical constraint may have a UID, but must have an operand sub-property:
	- **or** = one of constraints is satisifed
	- **xone** = only one, no more
	- **and** = all of constraints satisified
	- **andSequence** = all constraints in sequence 
	(requires serialisations to preserve the order)
So in the below example, the use of xone specifies you can either use the work online or print it, but not both:

```
{
    "@context": "http://www.w3.org/ns/odrl.jsonld",
    "@type": "Offer",
    "uid": "http://example.com/policy:88",
    "profile": "http://example.com/odrl:profile:10",
    "permission": \[{
        "target": "http://example.com/book/1999",
        "assigner": "http://example.com/org/paisley-park",
        "action": \[{
           "rdf:value": { "@id": "odrl:reproduce" },
           "refinement": {
               "xone": { 
	           "@list": \[ 
		       { "@id": "http://example.com/p:88/C1" },
                       { "@id": "http://example.com/p:88/C2" } 
		   \]
	       }
            }
        }\]
    }\]
}
 
{
   "@context": "http://www.w3.org/ns/odrl.jsonld",
   "@type": "Constraint",
   "uid": "http://example.com/p:88/C1",
   "leftOperand": "media",
   "operator": "eq",
   "rightOperand": { "@value": "online", "@type": "xsd:string" }
}
 
{
   "@context": "http://www.w3.org/ns/odrl.jsonld",
   "@type": "Constraint",
   "uid": "http://example.com/p:88/C2",
   "leftOperand": "media",
   "operator": "eq",
   "rightOperand": { "@value": "print", "@type": "xsd:string" }
}
```

### Refinement Property
Is a constraint/logical constraint that narrows the semantics of the **Action** (unlike Constraint Properties which are applied to a Rule). For e.g. in the example below, you can print up to 1200dpi resolution. 

```
{
    "@context": "http://www.w3.org/ns/odrl.jsonld",
    "@type": "Offer",
    "uid": "http://example.com/policy:6161",
    "profile": "http://example.com/odrl:profile:10",
    "permission": \[{
       "target": "http://example.com/document:1234",
       "assigner": "http://example.com/org:616",
       "action": \[{
          "rdf:value": { "@id": "odrl:print" },
          "refinement": \[{
             "leftOperand": "resolution",
             "operator": "lteq",
             "rightOperand": { "@value": "1200", "@type": "xsd:integer" },
             "unit": "http://dbpedia.org/resource/Dots\_per\_inch"
          }\]
      }\]
   }\]
}
```

## Rule class
A rule class is the parent of Permission, Prohibition and Duty classes. The Rule Class has the common attributes of each of them. It must have an Action property, and each class has its own duty associated with it (ie
	- the Prohabition Class, has a Remedy, which is a Duty
	- the Duty has a Consequences which is also a Duty
	- the Permission may have a Duty
	

### Permissions class
*A Permission allows an Action, with all Refinements Satisified, to be exercised on an Asset if Constraints are satisfied and Duties fulfilled.*

A permission must have a target Asset, and may have assigner/assignees or duties, but doesn't need them.

### Prohibition class
*A Prohibition disallows an Action, with all Refinements Satisified, to be exercised on an Asset if all constraints are satisfied. If the Prohibition has been infringed by the action being exercised, then any related Remedies MUST be fulfilled to set the state of the Prohibition to not infringed. *

A Permission must have a target Asset, and may have assigner/assignees or Remedies, but doesn't need them.

For e.g. below there is the Permission to display a photo archive, but not the permission to archive it.

```
{
    "@context": "http://www.w3.org/ns/odrl.jsonld",
    "@type": "Agreement",
    "uid": "http://example.com/policy:5555",
    "profile": "http://example.com/odrl:profile:08",
    "conflict": "perm",
    "permission": \[{
        "target": "http://example.com/photoAlbum:55",
        "action": "display",
        "assigner": "http://example.com/MyPix:55",
        "assignee": "http://example.com/assignee:55"
    }\],
    "prohibition": \[{
        "target": "http://example.com/photoAlbum:55",
        "action": "archive",
        "assigner": "http://example.com/MyPix:55",
        "assignee": "http://example.com/assignee:55"
    }\]
}
```

### Duty class
*A Duty is the onligation to exercise an action, with all refinements satisified. A Duty is Fulfilled if all constraints are satisfied and if its action has been exercised. If its action has not been exercised, then any Consequences must be fulfillfed to meet the Duty. *

e.g. you have a Duty to not park in this parking space, and failure to do so will create a new Duty to pay a fine.

A Duty may have a target Asset, and may have assigner/assignees or Consequences, but doesn't need them. The party obligated to perfrom a Duty must have the ability to exercise the Duty Action, and then must satisfy it.

In the below example there's an obligation between asigner and asignee to delete an asset; if they don't they must pay a fee of €10.

```
{
    "@context": "http://www.w3.org/ns/odrl.jsonld",
    "@type": "Agreement",
    "uid": "http://example.com/policy:42B",
    "profile": "http://example.com/odrl:profile:09",
    "assigner": "http://example.com/org:43",
    "assignee": "http://example.com/person:44",
    "obligation": \[{
	   "action": "delete",
	   "target": "http://example.com/document:XZY",
	   "consequence": \[{
	   	 "action": \[{
	   	     "rdf:value": { "@id": "odrl:compensate" },
	   	     "refinement": \[{
	   	        "leftOperand": "payAmount",
	   	        "operator": "eq",
	   	        "rightOperand": { "@value": "10.00", "@type": "xsd:decimal" },
	   	        "unit": "http://dbpedia.org/resource/Euro"
	   	     }\]
	   	 }\],
	   	 "compensatedParty": "http://wwf.org"
         }\]
    }\]
}
```

### Prohibition with remedy.. 
Below is an example where you are not allowed to index the data of a specific asset, but if you do, you must anonymise it.

```
{
    "@context": "http://www.w3.org/ns/odrl.jsonld",
    "@type": "Agreement",
    "uid": "http://example.com/policy:33CC",
    "profile": "http://example.com/odrl:profile:09",
    "prohibition": \[{
        "target": "http://example.com/data:77",
        "assigner": "http://example.com/person:88",
        "assignee": "http://example.com/org:99",
        "action": "index",
        "remedy": \[{
            "action": "anonymize",
            "target": "http://example.com/data:77"
        }\]
    }\]
}
```

## Policy Rule 
Policy rules allow a single rule to be related to multiple Assets, Partied and Actions. For e.g.  - multiple tracks and actions are allowed below:

```
{
    "@context": "http://www.w3.org/ns/odrl.jsonld",
    "@type": "Policy",
    "uid": "http://example.com/policy:8888",
    "profile": "http://example.com/odrl:profile:20",
    "permission": \[{
        "target": \[ "http://example.com/music/1999.mp3",
                    "http://example.com/music/PurpleRain.mp3" \],
        "assigner": "http://example.com/org/sony-music",
        "action": \[ "play", "stream" \]
    }\]
}
```

Policies can point to other policies and inherit rules from them (https://www.w3.org/TR/odrl-model/#inheritance).

### Policy Conflict

The Conflict propert is used to handle conflicts from merged policies/permissions/prohibitions. They can take three forms:
-   `perm`: the Permissions _MUST_ override the Prohibitions
-   `prohibit`: the Prohibitions _MUST_ override the Permissions
-   `invalid`: the entire Policy _MUST_ be void if any conflict is detected