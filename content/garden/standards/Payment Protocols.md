# Payment Protocols

Beside [[Web Payments]], the main protocol is [[ISO 20022]].

There's also Financial Products Markup Language, [FPML](https://www.fpml.org), for derivatives; [XBRL](https://www.xbrl.org/the-standard/what/an-introduction-to-xbrl/), the business and reporting standard language; 