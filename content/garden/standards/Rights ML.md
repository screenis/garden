# Rights ML

Rights ML is a Profile of [[ODRL]], developed by the IPTC, for any media type.
- Overivew: https://www.iptc.org/standards/rightsml/
- Dev site: http://dev.iptc.org/RightsML
- Specification: https://www.iptc.org/std/RightsML/2.0/RightsML_2.0-specification.html

## Example, taken from IPTC

_"It is permitted to make up to five printed copies of the content asset, for which a fee x must be paid in advance. It is permitted to make a further five printed copies, provided an additional fee x is paid in advance. It is prohibited to make more than ten printed copies in total."_

```
<o:policy xmlns:o="http://w3.org/ns/odrl/2" type="http://w3.org/ns/odrl/vocab#set" uid="http://example.org/id/a123456">
  <!-- The permission: allow printing up to 5 times, as long as a printing fee is paid first -->
  <o:permission xmlns:a="http://iptc.org/std/RightsML/2011-10-07/" xmlns:ns="http://example.org/myRightsMLvocabulary">
    <!-- The Content Asset that is the target of the permission -->
    <o:asset uid="ns:myContentAsset" relation="http://w3.org/ns/odrl/vocab#target"/>
    <!-- The action being permitted is printing -->
    <o:action name="o:print"/>
    <!-- The action may be performed less than or equal to 5 times -->
    <o:constraint name="o:count" operator="o:lteq" rightOperand="5"/>
    <!-- The action is permitted as long as a duty is fufilled -->
    <o:duty>
      <!-- The duty action is to make a payment -->
      <o:action name="o:pay"/>
      <!-- The duty entails paying a printing fee -->
      <o:asset uid="ns:printingFee" relation="http://w3.org/ns/odrl/vocab#target"/>
      <!-- The duty is constrained: the action can only occur during the time period whilst the policy is being exercised. -->
      <o:constraint name="o:event" operator="o:lt" rightOperand="o:policyUsage"/>
      <!-- The duty is constrained: the duty must be performed exactly once -->
      <o:constraint name="o:count" operator="o:eq" rightOperand="1"/>
      <!-- The "pay" duty must specify the payeeParty, i.e. who gets paid? -->
      <o:party function="o:payeeParty" uid="http://example.org/id/me"/>
    </o:duty>
  </o:permission>
 
  <!-- The prohibition: don't print more than 10 copies altogether -->
  <o:prohibition>
    <o:asset uid="ns:myContentAsset" relation="http://w3.org/ns/odrl/vocab#target"/>
    <o:action name="o:print"/>
    <o:constraint name="o:count" operator="o:gt" rightOperand="10"/>
  </o:prohibition>
</o:policy><br><strong></strong>
```
