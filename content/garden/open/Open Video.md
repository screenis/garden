# What is Open Video?

Perhaps the first question is [[What is Open]]?

### Organisations
 - [[Alliance for Open Media]]
 - [[C2PA]]
 - [[Content Authenticity Initative]]
 - [[Linked Content Coaltion]]

### Metadata standards
 - [[EIDR]]
 - [[Common Metadata]]
 - [[CORDRA]]
 - [[Extensible Metadata Platform (XMP)]] 
 - [[IMZ Metadata Standard]]

### Rights languages
- [[ODRL]]
- [[Rights ML]] 
- [[Linked Content Coaltion]]'s [RMM](http://www.linkedcontentcoalition.org/phocadownload/framework/The%20LCC%20Rights%20Reference%20Model%20v1.0.pdf).

### Licensing
- [[Copy Far Left]]
- [[Cryptographic Autonomy License]]

### Content Identifiers
- [[ISCC]] and its accompanying [[OCCP]] for certification. 
- Microsoft's [[Photo DNA]]. 
- [[Digital Object Identifiers (DOI)]], an implementation of [[Digital Object Architecture (DOA)]]

### Open Digital Asset Management Systems
- For distributed playback, there's [[Peer Tube]], a decentralised video hosting service using [[Activity Pub]].
- [[Islandora]]
- [[Phraseanet]]
- [[NextCloud]]
- [[WordPress DAM]]
- other [[Systems]]
- [[Digital Object Architecture (DOA)]] registries like [[CORDRA]].

### Tools
 - [Open Timeline](https://github.com/PixarAnimationStudios/OpenTimelineIO), 
 - [HTML/CSS/JS implementations of titles](https://biffud.com/tldr/bbc-ant-v1)
 - [[Hyperaudio]]
 - [[autoEdit]] - for transcript-based editing of documentary rushes

### Media production tools
 - [KDENLive](https://kdenlive.org/en) - video editor
 - [Shotcut](https://shotcut.org/) - video editor
 - [Olive](https://www.olivevideoeditor.org/) - video editor
 - [Kit Scenarist](https://kitscenarist.ru/en/index.html) - for screenplay writing
 - [Storyboarder](https://wonderunit.com/storyboarder/) - for creating storyboards

### Other links
- [[Content Moderation]]
- [[Video Wikipedia]]
- [[Open GLAM]]
- [[Open Source Governance]]