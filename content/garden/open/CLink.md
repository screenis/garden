# CLink Media

- https://clink.media
- https://what.clink.is

In 2020 Texas-based CLink launched "an interoperable infrastructure for asserting attributions and rights, processing licenses, disseminating content, and handling attributions for online content". The distributed system is built on [[Digital Object Architecture (DOA)]] and utilizes the entity model and framework of the [[Linked Content Coaltion]].

Their mission is to create 'an open federated infrastructure to facilitate legitimate commerce for online content including commerce in ownership to to usage rights and digital objects'

They have a WordPress plugin, and are a member of the [[Content Authenticity Initative]] and [[C2PA]]. They use [[ISCC]] for identifiers, with a focus on text/imageds.

### GFTW project - Monetization for authenticated content & rightsholders
 - appealing to NFTs
 - uses [[ISCC]]
 - uses [[ISNI]] for identifying creators
 - bonds a statement from an ISNI verified actor to an asset on a ledger 




### WordPress component
"the client component of a distributed system which first time integrates rights data; registry of persistent identifiers with metadata; peer-to-peer content licensing and delivery into a Content Management System, for personal use, on a non-business-related website."
https://wordpress.org/plugins/wpclink/

It's available uner GPL2.

### CLink Registry
https://clink.id/about.html

A [[Digital Object Architecture (DOA)]] registry built on [[CORDRA]](Content Object Repository Discovery and Resolution Architecture). Uses schemas are based on the Entity Model of the [Linked Content Coalition](http://www.linkedcontentcoalition.org/) validated over wide range of registries such as DOI, DDEX, EIDR, and ONIX.

The WordPress plugin provides one registration per day to the registry so can easily provide a path/url for the original file. 

**Worked example:** https://what.clink.is/overview

Has a metadata architect - Godfrey Rust.

### Content link

Registratio provides a content link in the footer of a WordPress post to allow licensed shares.

![[Pasted image 20210625173145.png]]