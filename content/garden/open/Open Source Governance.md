# Open Source Governance

## Good examples
### Electron
https://www.electronjs.org/governance

Working groups with a github page listing members, and recording all meeting notes. They use the terms:
-   A _maintainer_ is anyone who plays an active role in governance.
-   A _collaborator_ is active in the community, but not in governance.
-   A _participant_ is anyone who is a maintainer or collaborator.
-   A _working group_ is a group of maintainers that is formed to take responsibility for certain aspects of the Electron project. Normally these groups will meet regularly but in some cases will only meet as required to fulfill their responsibilities.
-   A [chair](https://github.com/electron/governance/tree/master/charter/README.md#Leadership) leads a working group.

**Code of conduct**: https://github.com/electron/governance/blob/master/CODE_OF_CONDUCT.md, based on the [Contributor Covenant](https://www.contributor-covenant.org/version/1/4/code-of-conduct/).