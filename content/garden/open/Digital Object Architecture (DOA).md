# Digital Object Architecture
A digital object is a work or portion of a work, conveyed in bits (like a digital entity as defined by the ITU). The architecture was envisioned by Robert E Kahn, co-inventor of TCP/IP. 

The architecture has three components:
 - identifier/resolution system (a [[UID]] for the object)
 - repository system (a location for the object)
 - registry system (a directory for the object)

This are defined by two protocols:
 - the Identifier/Resolution Protocol (IRP) - to resolve an identifier with current info like location, usage rights, timestampe etc.
 - Digital Object Interface Protocol (DOIP) - for digital object services like repositories and registries

It is governed by the [Dona Foundation](https://www.dona.net/), which was founded in Geneva in 2014. 

The specification is: https://www.dona.net/sites/default/files/2018-11/DOIPv2Spec_1.pdf



