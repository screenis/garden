# AutoEdit
https://www.autoedit.io/

For using a video transcript to copy/paste a rough cut and export and EDL. Based on BBC R&D's [Digital Paper Edit](https://github.com/bbc/digital-paper-edit-client/blob/master/docs/intro.md) ([slides](https://docs.google.com/presentation/d/1vVe_hgTj6JhLhU2WuZBOWx41ajcKX-8m8Xa0NIVZT2M/edit#slide=id.g6b51b79a88_0_523)) and developed by [Pietro Passarelli](http://pietropassarelli.com). Originally a Knight Mozilla fellowship project.