# Hyperaudio	

Tool by Mark Boas (https://maboa.it/) & Laurian Gridinoc (https://github.com/Laurian, https://about.me/gridinoc)

## Key links:

 - Convertor: https://hyperaud.io/converter/converter.html#
 - WordPress plugin: https://github.com/hyperaudio/wordpress-hyperaudio/tree/main/wp-hyperaudio
 - WordPress tutorial: https://www.youtube.com/watch?v=3Qpq8kj4PxM
 - New site: https://hyper.audio/
 - Old site: https://hyperaud.io/

#tools #openvideo #transcripts