# Common Metadata

Common Metadata is part of the MovieLabs Digital Distribution Framework (MDDF) to describe metadata relating to the sale and distribution of digital media. More info:
https://www.movielabs.com/md/md/v2.9/Common_Metadata_v2.9.pdf.

Common Metadata has two parts: 
 - **Basic Metadata** includes descriptions such as title and artists. It describes information  
about the work independent of encoding. 
 - **Digital Asset metadata** describes information about individual encoded audio, video and subtitle streams, and other media included. Package and File Metadata describes one possible packaging scenario and ties in other metadata types.  
Ratings and Parental Control information is described

![[Pasted image 20210926141155.png]]