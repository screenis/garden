# Alliance for Open Media

http://aomedia.org/

A consortium to deliver open video streaming technology, "AOMedia unites top tech leaders behind a collaborative effort to offer open, royalty-free and interoperable solutions for the next generation of media delivery".

Members range from Vimeo and Netflix to Samsung, Google, Intel and Microsoft.

Membership is available on request: http://aomedia.org/membership/.

