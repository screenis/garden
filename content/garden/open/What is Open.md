# What is Open?
Creative Commons includes NC to denote 'no-commercial use'. Initatives in recent years have seeked to reclaim 'Open' as something which cannot include this exclusion. Indeed most open source licenses differ only in the question of share-alike or not (GPL vs MIT). In other words, 'unless commercial exploitation of a working is allowed, then a work isn't open'.

This definition was pushed forward by the [Open Knowledge Foundation](https://okfn.org/), in their [Open Definition](https://opendefinition.org/od/2.1/en/), in relation to "Open Data, Open Content and Open Knowledge".

> It can be summed up in the statement that:
> “Open means **anyone** can **freely access, use, modify, and share** for **any purpose** (subject, at most, to requirements that preserve provenance and openness).”
> Put most succinctly:
> “Open data and content can be **freely used, modified, and shared** by **anyone** for **any purpose**”

[[Open GLAM]] use this same definition, as does much of the FOS world:
1 - http://www.opensource.org/osd.html  
2 - http://www.gnu.org/licenses/gpl.html  
3 - http://www.gnu.org/licenses/gpl-faq.html  
4 - http://oreilly.com/catalog/osfreesoft/book/

There has also been a push by some to get CC-BY-NC changed to make clear that it _is not Open_ (https://wiki.creativecommons.org/wiki/4.0/NonCommercial).

Simultaneously there has been a movement within Open Source licenses to carve out exceptions, such as commercial use. While there is considerable pushback (see [this thread of replies to a question on the subject](https://www.quora.com/What-is-the-best-free-for-non-commercial-use-license-to-use-for-an-open-source-project), responses range from the many [[Copy Far Left]] licenses to Microsoft's [Shared Source]([Shared Source](https://en.wikipedia.org/wiki/Shared_Source_Initiative)) and Heather Meeker's [Common Clause ](https://commonsclause.com/).

NB confusion shouldn't be made with the Open Source Initative [approved](https://opensource.org/licenses/alphabetical) [Non-Profit Open Software License version 3.0 (NPOSL-3.0)](https://opensource.org/licenses/NPOSL-3.0), which is for non-profits to release GPL code with fewer warranties than for-profits ([explainer]([Non-Profit Open Software License 3.0 (NPOSL-3.0)](https://opensource.org/licenses/NPOSL-3.0)).
