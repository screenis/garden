# Content Object Repository Discovery and Resolution Architecture (CORDRA)

https://www.cordra.org/

Cordra is an open source system for managing digital objects with resolvable identifiers at scale. It is based on the [[Digital Object Architecture (DOA)]]. Cordra can be deployed as a single instance or as a distributed system. It is neither a database or an index:

>Cordra offers a [unified programmable interface](https://www.cordra.org/documentation/api/introduction.html#api-introduction) across your [storage system](https://www.cordra.org/documentation/configuration/storage-backends.html#storage-configuration) and [indexing system](https://www.cordra.org/documentation/configuration/indexing-backends.html#indexing-configuration), and manages information logically as digital objects. Digital objects are typed, and Cordra enforces [schemas](https://www.cordra.org/documentation/design/schemas.html#types) on user-supplied JSON information.

## Features

>- Configurability. Schemas, business rules, access controls, and much more can be configured using settings and lifecycle hooks, all using the built-in admin interface.
>- Data Consistency. Enables robust internal machinery to withstand partial system/network failures to ensure consistent system states between storage and indexing services.
>- Scalability. Enables horizontal scaling and concurrency techniques to support large-scale demand. Provides a load-sharing front to backend services.
>- APIs. Provides REST, DOIP, and IRP interfaces. REST APIs reduce the entry barrier. DOIP operations allow extensibility and interoperability. IRP enables identifier resolution.
>- Developer Friendly. Requires little to no coding to store, protect, search, and retrieve digital objects. Client libraries in Java and JavaScript are provided.
>- PKI and Hashing. Server and client authentications can be handled via PKI in addition to passwords. Digital objects can be hashed and linked for ensuring integrity