# Copy Far Left

In contrast with [CopyLeft](https://www.gnu.org/licenses/copyleft.en.html), as defined by the GNU Project, [CopyFarLeft](https://wiki.p2pfoundation.net/Copyfarleft) seeks to limit commercial use to participants or contributors to the commons.

Some discussion around this (to read): https://www.metamute.org/editorial/articles/copyfarleft-and-copyjustright

**[CopyFarLeft / Peer Production License](https://wiki.p2pfoundation.net/Peer_Production_License)**
Published in the [Telekommunist Manifesto](http://telekommunisten.net/the-telekommunist-manifesto/) and is written by Dmytri Kleiner, co-author with the barrister John Magyar, It is a variation on CC-NC adding explicit provisions for non-profits, coops and 'commonsers', self identifying as not Copyleft so much as . It has a number of adopters including https://guerrillatranslation.wordpress.com/ & https://networg.wordpress.com/.

![Copyfarleft.jpg](https://wiki.p2pfoundation.net/images/Copyfarleft.jpg)

**[CopyFair License / Commons reciprocity](https://wiki.p2pfoundation.net/CopyFair_License)**
"aims to preserve the right of sharing knowledge without conditions, but aims to subject commercialization of any such knowledge commons to some form of contribution to that commons." The idea is also discussed in the Journal of Peer Production » -n° 4 jan. 2014) by Miguel Said Viera and Primavera de Filipi.

![Commons reciprocity licence.jpg](https://wiki.p2pfoundation.net/images/Commons_reciprocity_licence.jpg)

**[FairSource](https://fair.io/?a)**
Offers Open Source provisions for individuals and organisations up to a certain size. This size is defined with a number, so Fair Source 25 allows for organisations to use with a user limit of 25; beyond that a license fee kicks in. 

![Fair source.jpg](https://wiki.p2pfoundation.net/images/Fair_source.jpg)

[**FairlyShare**](https://wiki.p2pfoundation.net/FairlyShare)

"Type "Eiffel Tower" in Google. The first contents which you find come from Wikipedia, and it is flooded in the middle of sponsored links. Google thus monetizes commons in its profit. It's legal, but not sustainable. If Wikipedia was under license FairlyShare, Google should share fairly this profit with every contributor of the article on the Eiffel Tower."

"This principle was established 50 years ago by the Treaty of Rome of 1961. Since then, radios have to pay the composer-songwriters, whose music they broadcast, through bodies of collection. #FairlyShare is a modernized shape of this principle, which spreads the status of the authors to the contributors, that of radios to the developers of outbuildings, and which replaces the fixed key of distribution by algorithms of analysis of the tracks of the contributions."

![Fairlyshare.jpg](https://wiki.p2pfoundation.net/images/Fairlyshare.jpg)

**[la Fabrique des Mobilités proposal](https://wiki.p2pfoundation.net/Fab_Mob_Reciprocal_License_for_the_Legal_Contractualisation_of_Commons)**
aka "Fab Mob Reciprocal License for the Legal Contractualisation of Commons", based on [la Fabrique des Mobilités](https://wiki.lafabriquedesmobilites.fr/wiki/Accueil) - is another approach to limit unrestricted use to members of a collective, commune or cooperative.

![FabMob-reciprocal-licence.jpg](https://wiki.p2pfoundation.net/images/FabMob-reciprocal-licence.jpg)

*Images by the [P2P Foundation](https://wiki.p2pfoundation.net/Fab_Mob_Reciprocal_License_for_the_Legal_Contractualisation_of_Commons)*