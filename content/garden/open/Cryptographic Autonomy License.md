# Cryptographic Autonomy License

- License: https://opensource.org/licenses/CAL-1.0
- Press release: https://holo.host/wp-content/uploads/CAL-First-Open-Source-license-designed-for-Decentralized-Apps.pdf
- Introduction by its author Van Lindberg https://www.processmechanics.com/2019/03/15/the-cryptographic-autonomy-license/

CAL is a copyleft license like the GPL but adds an extra provision around what you can and can't do with a user's data. It was created by Holo, the coin associated with [[Holochain]]. It's "the first approved Open Source license that [explicitly does anything significant about data](https://medium.com/holochain/why-cal-is-important-to-the-end-user-aec58b2ff730)" as well as the first FOS license for decentralised Apps (where users process data locally).

>In addition to providing each Recipient the opportunity to have Access to the Source Code, You cannot use the permissions given under this License to interfere with a Recipient’s ability to fully use an independent copy of the Work generated from the Source Code You provide with the Recipient’s own User Data. 
Section 4.2 Maintain User Autonomy: intro text

In other words you cannot use the software to keep the user from accessing their *own* data. It's a bit like a GDPR subject access request baked into every code licensed under CAL. So if your software processes user-data, this data cannot be kept from each user it comes from. And if you fork a CAL project, and use it to process user data with another proprietary tool, you cannot ultimately hide this processed data from each user.

"It protects the rights’ of end users of distributed and cloud-based apps: ensuring users’ ownership of their data, the ability to operate the software independently, and control of their keys."

A good explanation and an example here:
https://blog.jwf.io/2020/10/cryptographic-autonomy-license-cal-1-0/

> "I am a product manager at Hatbrim Technologies. Hatbrim develops an integrated calendar application, **Holocal**, to store events, meetings, and reminders. Holocal is an integrated application that includes a front-end component, back-end component, and a machine learning algorithm. The algorithm offers tailored suggestions to reduce my meeting load based on my common meeting patterns with other events or activities I have planned.
> Oraculous, a competing company to Hatbrim Technologies, creates a fork of Holocal called **OraCal**. It is almost functionally identical to Holocal except it also adds an integration to other services from Oraculous. However, OraCal also modifies the calendar optimization algorithm. It adds a periodic random event suggestion based on events and activities in your calendar.…
> Eventually, one developer runs OraCal internally, but optimized for our data. Still no luck to reproduce the nifty calendar event suggestion feature! Fortunately, the CAL-1.0 offers a protection here. So, the developer sends an email to Oraculous to request her personal user data from OraCal provided to her. Because the CAL-1.0 has provisions to prevent foul play or modifying the data, the developer receives a copy of her data and realizes another Oraculous tool was scrubbing and appending data for calendar predictions before it returned to OraCal."

Maybe a simpler explanation would be that 'open sourcing Facebook' under CAL would not only put the code under an open license but ensure every user got a copy of their own data. Without the CAL it would just provide a blank Facebook to start from scratch; Facebook could still own the data generated and hosted with the app.

### Backlash
OSI co-founder resigned over it's inclusion into the OSI's list of open licenses. https://www.theregister.com/2020/01/03/osi_cofounder_resigns/
http://lists.opensource.org/pipermail/license-review_lists.opensource.org/2020-January/004616.html