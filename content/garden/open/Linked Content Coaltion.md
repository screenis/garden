# Linked Content Coalition

http://www.linkedcontentcoalition.org/

LCC is a not-for-profit global consortium of standards bodies and registries, who create and manage content data standards, particularly for identifiers, metadata and messaging. The editor of the framework was [Godfrey Rust](http://rightscom.com/the-team/godfrey-rust/) of [RightsCom](http://rightscom.com/), who also consulted on the [[DOI]] – and it was intended to be the basis for the stalled UK Copyright Hub.

## Targets
They have 10 targets (quoted below from [here](http://www.linkedcontentcoalition.org/index.php/rights-data-network/10-targets)):

1. [A global Party ID](http://www.linkedcontentcoalition.org/index.php/rights-data-network/10-targets/74-01-a-global-party-id). Rightsholders and “asserters” should be identified with an identifier linked to the ISNI “hub”.
2. [Creation IDs for all](http://www.linkedcontentcoalition.org/index.php/rights-data-network/10-targets/73-02-creation-ids-for-all). Creations of all types should be identified to any required level of granularity.
3. [Right IDs](http://www.linkedcontentcoalition.org/index.php/rights-data-network/10-targets/72-03-right-ids). Content rights should be identified distinct from, but linked to, the Creations to which they relate.
4. [Resolvable IDs](http://www.linkedcontentcoalition.org/index.php/rights-data-network/10-targets/71-04-resolvable-ids). Identifiers should have a URI form so that where they may be persistently and predictably resolved to multiple services within the internet.
5. [Linked IDs](http://www.linkedcontentcoalition.org/index.php/rights-data-network/10-targets/70-05-linked-ids). “Cross-standard” links between identifiers should use interoperable terms and be authorised by interested Parties at both ends of the link.
6. [Interoperable metadata](http://www.linkedcontentcoalition.org/index.php/rights-data-network/10-targets/69-06-interoperable-metadata). Standard content and rights metadata schemas and vocabularies should have authorised, public mappings which enable terms and data to be automatically transformed from one standard into another.
7. [Provenance of Rights data](http://www.linkedcontentcoalition.org/index.php/rights-data-network/10-targets/68-07-provenance-of-rights-data). The provenance (“asserter”) of Rights declarations should be made explicit.
8. [Digital Rightsholder Statement (“DRS”)](http://www.linkedcontentcoalition.org/index.php/rights-data-network/10-targets/67-08-digital-rightsholder-statement-drs). Anyone should be able to make standardised, machine-interpretable public statements about rightsholdings in Creations.
9. [Conflict Management](http://www.linkedcontentcoalition.org/index.php/rights-data-network/10-targets/66-09-conflict-management). Conflicts between public rights declarations should be automatically identifiable so that their resolution can be managed.
10. [Linked Fingerprints](http://www.linkedcontentcoalition.org/index.php/rights-data-network/10-targets/65-10-linked-fingerprints). Where digital “fingerprints” or embedded “watermarks” exist, they should be mapped to registered Creation identifiers.

## LCC Framework

> The LCC project set out to define:
>1. the types of things that occupy the network, and their relationships: Rights Reference Model;  
>2. how to identify things in the network: Principles of Identification;  
>3. how the rights data passes through the network: Principles of Messaging;

There is fuller details of the entity model here: http://www.linkedcontentcoalition.org/phocadownload/framework/The%20LCC%20Entity%20Model%20v1.0.pdf

### 1.  The LCC Rights Reference Model (RRM)

The LCC Rights Reference Model (RRM) ([http://doi.org/10.1000/284](http://www.linkedcontentcoalition.org/phocadownload/framework/The%20LCC%20Rights%20Reference%20Model%20v1.0.pdf)) is a formal, general and extensible reference data model for representing intellectual property rights and entitlements for any media or content. This was implemented in the now defunct https://www.rdi-project.org/.

Presentation from 2015: https://www.europarl.europa.eu/cmsdata/80807/farrow.pdf

### 2. The LCC Principles of Identification

### 3. The LCC Principles of Messaging