# Open GLAM
Galleries, libraries, archives and museums.

[OpenGlam.org](https://openglam.org/) is a network of institutions and people developing policies and practices on ethical open access to cultural heritage. 

They are supported by Creative Commons and the Wikimedia Foundation, and publish a Medium space: https://medium.com/open-glam.

Their [_Declaration on Open Access for Cultural Heritage_](https://openglam.pubpub.org/) is being developed and in consultation/draft. Not much has been published yet but Andrea Wallace has written an [Executive Summary](https://openglam.pubpub.org/pub/executive-summary/release/2) saying:

> "Cultural heritage institutions face a number of obstacles to digitizing and making collections available online. Many are beyond their control. But there is one important area that these institutions _do_ have control over: the access and reuse parameters applied to a breadth of media generated during the reproduction of public domain works.

They take a strong line on what "open" means; in short any work restricting commerical use cannot be open ([ref](https://openglam.pubpub.org/pub/the-glossary/release/1))
**"Prohibitions of commercial reuse disqualify a policy or practice from being characterized as open."**

She continues that the declarations starting points should be:
- Embracing the premise that **no new rights should arise in non-original reproduction media of public domain cultural heritage**; 
- Establishing consensus to **align “open” with international definitions allowing commercial use**, as well as the appropriate application of open licenses, tools, and labels;
- Introducing **greater nuance to open access** to invite, enable, and embrace newcomers who bring critical insights into what “open” means in the cultural heritage ecosystem;
- Providing adequate space within OpenGLAM to facilitate ongoing and collaborative improvements to **appropriate management of digital heritage** and **expand knowledge around emerging questions in open GLAM**; 
- Recognizing the **need for a baseline document that redefines and reclaims open GLAM** as a shared philosophy, practice, conversation, and space framed broadly as a _Declaration on Open Access for Cultural Heritage_; and
- Moving into **a phase of critical open GLAM** that interrogates current systems, centers reflexive and locally-informed approaches, and strengthens the network to support GLAM staff and wider communities around good practice in digital heritage management and engagement.
