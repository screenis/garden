# Next Cloud
Aims to be a full intranet (chat, groupware, calendar, files, etc) as well as file management. Can be run:
 - NextCloud Hub - the main product. Can be SaaS hosted in the cloud, or local hosted
 - Devices - can be bought pre-configured https://nextcloud.com/devices/

**Files**
Nextcloud files are stored in conventional directory structures, accessible via [WebDAV](https://en.wikipedia.org/wiki/WebDAV "WebDAV") if necessary. User files are encrypted during transit and optionally at rest.
- Collaborative file mgmt, with access control, file locking
- Can create workspaces inside folders with files, and discussion around those files.
https://nextcloud.com/files/
- Flows can run processes in workflows - e.g. convert to PDF or scripts (https://apps.nextcloud.com/apps/workflow_script) - why not? - generate ISCC.
- Secure file exchange

**Streaming media** 
Is available via [Ampache](https://en.wikipedia.org/wiki/Ampache).
- For music that's used in [NextCloud Music](https://github.com/owncloud/music) and the [AudioPlayer](https://github.com/Rello/audioplayer).
- For video there's no native support, but [some reports](https://help.nextcloud.com/t/streaming-hd-videoclips-buffering-takes-ages-while-downloading-the-entire-file-is-quick/50072/3) that it's slow to some devices.

**Video conversion**
via a plugin: https://apps.nextcloud.com/apps/video_converter - supporting MP4, AVI, WEBM & M4V.

**Case study** for use by IVZ for ArD's digital video infrastructure:
https://nextcloud.com/media/wp135098u/CaseStudy-IVZ_Web.pdf

