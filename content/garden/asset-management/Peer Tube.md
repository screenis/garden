# PeerTube

https://joinpeertube.org/

Federated video hosting, similar & compatible with Mastadon, created by a French non-profit [FramaSoft](https://framasoft.org/en/) using [[Activity Pub]].

Has option of Torrent streaming and torrent downloads to reduce bandwidth costs; can control levels of federation (prevent federation; block specific sites, etc).

How to install: https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md (Nginx)

## Architecture

Includes…
-   a modern database engine (PostgreSQL) to store long-term metadata
-   a reverse proxy (we officially support Nginx but nothing prevents us from using others) to handle certificates, and directly serve static assets
-   a key-value store (Redis) to help application caching and task queueing
-   a server application with:
    -   controllers to server static and clients files
    -   a REST API server providing the actual logic
    -   an ActivitPub API to receive messages and present local objects
    -   a BitTorrent tracker to allow clients to do P2P
    -   a socker.io server to notify clients
    -   a task scheduler to send ActivityPub requests, run transcoding jobs etc.
-   a _Single Page Application_ web client that consumes the REST API
-   a user that interacts with the web client of his choice

![architecture overview](https://docs.joinpeertube.org/assets/architecture-overview.jpg)

### Technologies
-   [TypeScript](https://www.typescriptlang.org/) -> Language
-   [PostgreSQL](https://www.postgresql.org/) -> Database
-   [Redis](https://redis.io/) -> Job queue/cache
-   [Express](http://expressjs.com) -> Web server framework
-   [Sequelize](http://docs.sequelizejs.com/en/v3/) -> SQL ORM
-   [WebTorrent](https://webtorrent.io/) -> BitTorrent tracker and torrent creation
-   [Mocha](https://mochajs.org/) -> Test framework

## Requirements

https://joinpeertube.org/faq#should-i-have-a-big-server-to-run-peertube 

"PeerTube should run happily on a virtual machine with 2 threads/vCPUs, at least 1 Gb of RAM and enough storage for videos. In terms of bandwidth, a lot will depend on which PeerTube instances you federate with and what your relation with them is (more about that below). As a real life example, the PeerTube demonstration server https://peertube.cpy.re runs on 2 vCores and 2GB of RAM. 

Average consumption is:
-   CPU: nginx ~ 2%, peertube ~ 10%, postgres ~ 1%, redis ~ 1%
-   RAM: nginx ~ 1MB, peertube ~ 150MB, postgres ~ 30MB, redis ~ 20MB
-   Network: ~200GB sent per month (https://framatube.org: ~1.5TB sent per month)"