# Pimcore

Like [[NextCloud]] - it does alot: CRM, CMS and more, and using PHP/Symfony. 

[Pimcore DAM](https://pimcore.com/en/platform/digital-asset-management/features/image-video-conversion) has mgmt, transcoding, seems more for media assets than video, but uses FFMPEG so supports https://www.ffmpeg.org/general.html#File-Formats 

Has a community edition - https://pimcore.com/en/platform/community-edition.

Github - https://github.com/pimcore/pimcore

