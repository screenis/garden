# Fedora
https://getfedora.org/

"Fedora creates an innovative, free, and open source platform for hardware, clouds, and containers that enables software developers and community members to build tailored solutions for their users."

Not sure what that means. 

But it's a Linux distro for desktop to server to cloud to IoT device to datacentre. 
And according to https://duraspace.org/fedora/ it's "the flexible, modular, open source repository platform with native linked data support". I

t was sponsored by Red Hat (hence the hat reference), has 1.2 million users and includes Linus Torvalds amongst its users ([ref](https://www.zdnet.com/article/look-whats-inside-linus-torvalds-latest-linux-development-pc/), alongside Linus' PC specs).