# Islandora
[Islandora](https://islandora.ca/index.php/) allows Drupal users to view and manage digital objects stored in a [[Fedora]] Repository, with Solr for search.. seems good for scale.