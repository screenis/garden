# DAM inside WordPress

Most will hit the server's PHP file upload limit.

Internal media asset management:
- **FILR** - file management with access control, $69-$499. https://patrickposner.dev/plugins/filr/
- **All-in-one Video Gallery**. Adds videos as posts, handles local and embeded. https://wordpress.org/plugins/all-in-one-video-gallery/. Free/paid verions.
- **Spreebie Transcoder** – resizes and compresses video. Requires FFMPeg, and integrates with [WP Real Media Library](https://wordpress.org/plugins/real-media-library-lite/) for folder sorting. Videos can be archived on Google Cloud. https://wordpress.org/plugins/spreebie-transcoder/

**As an API on external service**
- [[Phraseanet]] - one of the few plugins for a FOSS DAM. https://www.alchemy.fr/en/plugin-wordpress-phraseanet/
- [Cloudinary](https://cloudinary.com/) is an API for transcoding (bitrate, format) and resizing, with CDN, and is available as a WP plugin: https://nb.wordpress.org/plugins/cloudinary-image-management-and-manipulation-in-the-cloud-cdn/. But Cloudinary hosts your media on their servers, and serves over their CDN. Pricing from free (25k transformations/25gb storage - to $249/month - 600k transformations/600GB).
- [Brandfolder](https://brandfolder.com/). Works in the same way.
- **Transcoder** from [rtMedia](https://retmedia.io) - transcodes 3g2, 3gp, avi, flv, m4v, mp4, mpg, ogv, webm, wmv. https://wordpress.org/plugins/transcoder/. Free plan - 100mb/file, 5gb/month or Silver plan - $9/month - 16gb upload & 100GB/month.
- [Isset.video](https://isset.video/). Same.
- 
**And also**
- Presto Player - more video player than DAM. https://wordpress.org/plugins/presto-player/
- Broadcast Live Video. Badly reviewed but highly specced: https://wordpress.org/plugins/videowhisper-live-streaming-integration/