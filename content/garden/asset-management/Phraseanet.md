# Phraseanet
an Open Source Digital Asset Management solution published by [Alchemy](https://www.alchemy.fr/en/rubrique/societe/) to manage, publish and share digital media resources (pictures, videos, audio, PDF, Office documents…).

https://github.com/alchemy-fr/Phraseanet & https://www.phraseanet.com/en/download/

- Claims to be "The most complete Open Source Digital Asset Management solution on the market". GPL v3. Has a free [[WordPress DAM]] plugin. [Drupal plugin](https://www.alchemy.fr/en/drupal-phraseanet-plugin/) costs €1900. (other plugins for Magento/Powerpoint). French, English, German, Dutch versions.
- Can be installed locally or with Docker. 
- Connects with Front End apps with [Parade](https://www.alchemy.fr/en/parade/).
- Support contracts from €1800-7800. Or 10h support for €850.
- Supports Windows Video, Quicktime & H264. No transcoding.
- Used widely in France:
	-   City halls : Paris, Nice, Toulouse, Grenoble, Bordeaux, Montpellier…
	-   Monoprix, VentePrivee, Vinci, Sanofi Aventis, TF1, France Télévisions, Canal+…
	-   Education: Insead, Polytechnique, Lille University…

There are a number of certified service providers:
 - https://www.activo-consulting.com/
 - https://www.smile.eu/fr
 - https://www.ripconsulting.fr/
 - https://www.msi.nc/
 - http://www.intellia.ma/