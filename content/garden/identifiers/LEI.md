# Legal Entity Identifier (LEI)

Is an ISO standard recognised by the G20 for legal entities such as companies, foundations, charities, etc. LEIs are issued by LEI Issuers. 

For e.g. Bloomberg: https://lei.bloomberg.com/ – charges $65 for registration and then $50/year.

https://www.gleif.org/en/about-lei/introducing-the-legal-entity-identifier-lei

https://www.gleif.org