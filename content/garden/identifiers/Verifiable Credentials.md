# Verifiable Credentials

"A verifiable credential (VC) is an open standard ([W3C Verifiable Credentials](https://www.w3.org/TR/vc-data-model/)) for digital credentials. Verifiable credentials can represent information found in physical credentials, like a badge or a license, or new things that have no physical equivalent, such as ownership of a bank account. VCs are digitally signed, which makes them tamper-resistant and instantaneously verifiable and are similar to [[Self-sovereign Identity]].

Credentials can be issued by anyone about anything and be veriied by everyone. "The entity that generates the credential is called the **Issuer**. The credential is then given to the **Holder** who stores it for later use. The Holder can then prove something about themselves by presenting their credentials to a **Verifier**. This is validated via a data registry which the issuer writes to and the verifier checks against.

> It’s like a shop clerk in Boston, MA deciding if they should accept a license issued by the state of Hawaii as proof of age when purchasing alcohol.

More info: https://verifiablecredential.io/learn