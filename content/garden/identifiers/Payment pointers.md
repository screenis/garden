# Payment Pointers

"It should be simple for individuals or small companies to host their own payment service endpoints at a URL that can be resolved via a simple and easily recognizable payment pointer."
https://paymentpointers.org/goals/

So, for e.g. $mercurytheater.com/citizen-kane could resolve to a payment ID for that film, or with $mercurytheater.com/orson-wellles, the person.

A payment pointer can be used bi-directional, to both send and receive money. It resolves to [[Open Payments]] information to do this

![[Pasted image 20210617010936.png]]