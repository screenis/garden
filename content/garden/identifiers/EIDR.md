# Entertainment Identifier Registry (EIDR)
The Entertainment Identifier Registry Association (EIDR) is a not-for-profit industry association that was founded by MovieLabs, CableLabs, Comcast and TiVo to create universal identifiers (EIDRs) for audio visual objects (Features, TV shows, TV series, seasons and episodes; Short films; Promotional material (ads, trailers); Interactive material).
Registrants are approved industry companies paying $6,000 to $60,000 to get EIDRs IDs, a [[Decentralized Identifiers]], which looks like:

![](https://www.eidr.org/wp-content/uploads/2010/09/Standard-EIDR1.jpg)

EIDR for an object consists of a standard registry pre-fix, the unique suffix for each asset and a check digit. 

EIDR is operated on a non-profit cost-recovery basis. Members are allowed to maintain full copies of the Registry as needed. A Technical Advisory Board consisting of representatives from Ascent, Deluxe, Disney, Netflix, Warner Bros, Rovi and MovieLabs guides the organization on matters relating to development, roadmap, etc.

> EIDR is purely functional without any implication of ownership, making it persistent enough to remain the same despite any change in control or ownership of the underlying asset.

### Co-operation

> "EIDR is founded on the principle of open participation and welcomes all ecosystem players (commercial and non-profit) to join the Registry as registrant, lookup user or even a promoter. The Registry is intended to provide a foundational namespace for A/V objects that can be leveraged by participant in the eco-system to further their own business needs and offerings."

- Search: https://ui.eidr.org/search
- The data model: https://www.eidr.org/documents/Introduction%20to%20the%20EIDR%20Data%20Model.pdf
- Fully data field spec: https://www.eidr.org/documents/EIDR%202.6%20Data%20Fields%20Reference.pdf
- Registry technical overview: https://www.eidr.org/documents/Registry%20Technical%20Overview.pdf
