# Proof of Humanity
https://www.proofofhumanity.id/
https://twitter.com/proofofhumanity

An [[Identity]] blockchain with a methodology, "combining webs of trust, with reverse Turing tests, and dispute resolution to create a sybil-proof list of humans." Created by https://twitter.com/santisiri

It pays out a coin every hour to every registered human. But it's based on Ethereum so has an unworkable carbon footprint for now.

Sidenote - Vitalik Buterin— the creator of Ethereum—on OCtober 20th 2021, bought $200k’s worth of [the UBI coin powering Proof of Humanity](https://twitter.com/daily_ubi_bot) and burned it, taking it out of circulation for good. The result? The UBI coin’s value skyrocketed—from an all-time low of USD $0.02 on Sep 26th to an all-time high of USD $1.39 on Oct 20th.