# Depict

A framework encompassing the [[ISCC]] with [[Decentralized Identifiers]] and the Open Content Certification Protocol ([[OCCP]]).

![DE:PICT Framework](https://miro.medium.com/max/793/1*C0kkdyTh2P0eTdBXIDJvKQ.png)