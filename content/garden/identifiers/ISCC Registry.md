# ISCC Registry
Author: Titusz Pan
https://github.com/titusz/iscc-registry

### How to create a Decentralized Registry?

The idea is to design an open cross-blockchain registration protocol. Exercising the protocol materializes a global and publicly auditable meta-registry with short and unique content identifiers that bind digital content to actor identities. The architecture supports the discovery of identical and similar content registered by different actors across different blockchains. The protocol is quite simple:

-   Generate a full ISCC Code
-   Choose any shared public ledger that has a protocol implementation
-   Register your ISCC with a signed transaction on the chosen ledger

The ISCC Short-ID binds digital content to actor identities. The Short-ID emerges naturally from DLT transactions that are: Public, Authenticated, Ordered, Immutable. Anybody who follows the cross-chain indexing protocol can build the state of the full ISCC Short-ID Index (meta-registry). The same ISCC Code may be registered by multiple actors on different chains. They all get different but matchable Short-IDs. A meta-registry resolves ISCC Short-IDs to a transaction (or its information) on a given chain that carries the full ISCC code and links to more metadata or the content itself on IPFS/Filecoin or elsewhere.

### Proof of concept

A tool was created that registers ISCC codes on Ethereum and publishes metadata to IPFS