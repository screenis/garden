# Content Blockchain
https://www.content-blockchain.org.
A collaboration with Sebastian Posth and Titusz Pan, with inital funding from the Google News Initative. It had four outputs:
 - the [[ISCC]].
 - [smart licenses](https://smartlicense.coblo.net/)
 - Blockchain explorer
 - Desktop Wallet

Writeup of Titusz Pan 2019 talk Con 2019: Berlin, Blockchain for Science (https://www.youtube.com/watch?v=4OCvPrDhGuQ)

The paradigm shift:

![[Pasted image 20210602180103.png]]

The content blockchain

![[Pasted image 20210602180215.png]]

> Goal: Develop open standards, techs and applications that establish content as the subject of transactions on blochains.

![[Pasted image 20210602182315.png]]

Decentralized Content-based Identification

![[Pasted image 20210602182754.png]]