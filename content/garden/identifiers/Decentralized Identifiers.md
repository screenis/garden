# Decentralized Identifiers (DIDs) v1.0

DIDs, a type of [[Self-sovereign Identity]], are [URI]s that combine benefits from URLs and URNs.

- Main spec: https://www.w3.org/TR/did-core/

> "[Decentralized identifiers](https://www.w3.org/TR/did-core/#dfn-decentralized-identifiers) (DIDs) are a new type of identifier that enables verifiable, decentralized digital identity. A [DID](https://www.w3.org/TR/did-core/#dfn-decentralized-identifiers) refers to any subject (e.g., a person, organization, thing, data model, abstract entity, etc.) as determined by the controller of the [DID](https://www.w3.org/TR/did-core/#dfn-decentralized-identifiers). In contrast to typical, federated identifiers, [DIDs](https://www.w3.org/TR/did-core/#dfn-decentralized-identifiers) have been designed so that they may be decoupled from centralized registries, identity providers, and certificate authorities. Specifically, while other parties might be used to help enable the discovery of information related to a [DID](https://www.w3.org/TR/did-core/#dfn-decentralized-identifiers), the design enables the controller of a [DID](https://www.w3.org/TR/did-core/#dfn-decentralized-identifiers) to prove control over it without requiring permission from any other party. [DIDs](https://www.w3.org/TR/did-core/#dfn-decentralized-identifiers) are [URIs](https://www.w3.org/TR/did-core/#dfn-uri) that associate a [DID subject](https://www.w3.org/TR/did-core/#dfn-did-subjects) with a [DID document](https://www.w3.org/TR/did-core/#dfn-did-documents) allowing trustable interactions associated with that subject."

- Spec co-author, https://www.evernym.com/ - commercial Digital Identity provider. Consumer offering: https://connect.me/
- Spec co-author, https://digitalbazaar.com - does similar
- Spec co-author, https://danubetech.com - Decentralised Identify Foundation

![Decentralized Identifier example](/media/20210503154201.png)
