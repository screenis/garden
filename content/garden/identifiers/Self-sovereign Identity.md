# Self-Sovereign Identity

"**Self-sovereign identity (SSI)** is an approach to digital [[Identity]] that gives individuals control of their digital identities. SSI addresses the difficulty of establishing trust in an interaction. In order to be trusted, one party in an interaction will present credentials to the other parties, and those relying parties can verify that the credentials came from an issuer that they trust. In this way, the verifier's trust in the issuer is transferred to the credential holder. This basic structure of SSI with three participants is sometimes called "the trust triangle".

 - https://en.wikipedia.org/wiki/Self-sovereign_identity
 - https://ssimeetup.org/

The EU is creating an [eIDAS](https://en.wikipedia.org/wiki/EIDAS) compatible European Self-Sovereign Identity Framework (ESSIF). The ESSIF makes use of [[Decentralized Identifiers]] (DIDs) and the European Blockchain Services Infrastructure (EBSI). ref: https://ssimeetup.org/understanding-european-self-sovereign-identity-framework-essif-daniel-du-seuil-carlos-pastor-webinar-32/

Related to this is the idea of [[verifiable credentials]]

[Philip Sheldrake](http://philipsheldrake.com/), has written a strong critique of the risks of SSI as part of the Generative Identity book:
https://generative-identity.org/the-dystopia-of-self-sovereign-identity-ssi/

> Put starkly, many millions of people have been excluded, persecuted, and murdered with the assistance of prior identity architectures, and no other facet of information technology smashes into the human condition in quite the same way as ‘digital identity’. Therefore, if ever there’s a technological innovation for which ‘move fast and break things’ is not the best maxim, this is it. We need to move together with diligent respect for human dignity and living systems.

A further warning from Nathan Schneider:

> "... we cannot accept technology as a substitute for taking social, cultural, and political considerations seriously. Decentralized technology does not guarantee decentralized outcomes.""

[[pro]]