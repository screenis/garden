# Norsk Web Design

Categorised by simple bold colours, large simple menus typically of 3 items (or with a menu button for more), sans-serif fonts, very accessible design, feels mobile-first - with full width header & footer and narrower body.

For e.g.

- https://vegascene.no/
- https://www.greenhouseoslo.no/
- https://www.oslo.kommune.no/#gref
- https://www.helsenorge.no/en/information-in-english/
- https://www.uks.no/

You do see a similar aesthetic in, say, International art, e.g. [e-flux](https://www.e-flux.com/) in New York; but Norsk seems even simpler and pared back.