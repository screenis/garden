# DIMPACT

[DIMPACT](https://dimpact.org) is a collaborative project, based at the University of Bristol designed to develop an online tool that takes the complexity out of calculating the carbon emissions of the downstream value chain of digital media content. Collaborators include sixteen media giants including [[Netflix]], Sky, Channel 4, the BBC, ITV and Informat

More info: [wired.co.uk/netflix-footprint](https://www.wired.co.uk/article/netflix-carbon-footprint).

**Calculations**
- 88-93g CO2e – viewing over streaming, cable and satellite platforms used a mean of 0.17–0.18 KWh per device-hour (88–93 gCO2e) while terrestrial broadcast used a mean of 0.07 kWh (36 gCO2e). https://www.sciencedirect.com/science/article/pii/S0195925521001116?via%3Dihub
- Netflix / Dimpact study claims file size made little difference 'because the Internet is laways on'.. "Changing from standard definition to 4K resolution increases emissions from just under 1g CO2e/hour to just over 1g CO2e/hour. Why? The internet is “always on,” so the additional energy it takes to transmit higher resolution to your TV is marginal compared to the energy it takes to constantly operate the internet." https://about.netflix.com/en/news/the-true-climate-impact-of-streaming
