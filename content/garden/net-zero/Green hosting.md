# Green hosting

Worth differntiating between hosting *powered* by renewable power; and hosting *offset* with renewable energy credits.

The **Green Web Foundation** has a directory of green hosting providers, although doesn't distinguish: https://www.thegreenwebfoundation.org/directory/.

- TV streaming platform Zattoo, with up to 350,000 users per day, has started putting data centres inside wind-turbines for direct clean energy - https://zattoo.com/de/en/zattoo-and-windcores. This is a first step to testing if the energy is solid enough, given wind is variable.