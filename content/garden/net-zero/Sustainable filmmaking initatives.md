# Sustainable filmmaking initatives

There's a number of initatives and orgs trying to resolve filmmaking's large energy, CO2 and resource footprints.
- [Project Albert](https://wearealbert.org/), UK. Industry funded sustainability intiative, with extensive resources, training, and 'Sustainable Production Certification (1/2/3 star)'.
- [Global Green Media Network](https://www.globalgreenmedianetwork.com/project-description)
- [Filmmakers for the the Future:](https://filmmakersforfuture.org/en/). Campaigning, events and some resources.
- [Grüner Drehpass](https://www.ffhsh.de/de/film_commission/gruener_drehpass.php.), Germany. Green Shooting Initiative of the Hamburg Film Fund . 
- [Green Film](https://www.green.film/), Italy. Rating system for sustainable film production.
- [Green Shoting](https://greenshooting.mfg.de/), Germany. Green Shooting Initiative of the Baden-Württemberg Film Fund.
- [Creative Industries Pact for Sustainability](https://creativeindustriespact.com/), Pact to drive collective progress toward climate action in the creative industries.
- [Adgreen](https://www.adgreen-apa.net/), UK. 'supports the advertising industry’s transition to environmentally sustainable production methods'.
- [Green Film Tools](http://greenfilmtools.com/), Germany. Knowledge database and workshops on environmentally friendly production techniques.
- [EcoProd](http://www.ecoprod.com/fr/)
- [BBC's targets](https://www.digitaltveurope.com/2021/10/25/bbc-and-itv-announce-climate-pledges/)

### Reading list
**Hollywood's Dirtiest Secret** - The Hidden Environmental Costs of the Movies
by Hunter Vaughan, Columbia University Press, 2019
https://cup.columbia.edu/book/hollywoods-dirtiest-secret/9780231182416