# Net zero internet video?

> [“There are no passengers on Spaceship Earth. We are all crew.”](https://www.sciencedirect.com/book/9780750678889/paradigms-lost)
Marshall McLuhan

How to calculate
- [[Modelling]]
- [[Digital CO2 analysis]]
- [[Scope 1, 2 & 3]] - how wide a scope should be calculated?
- [[Lifecycle analysis]] - why it's more than just CO2eqv emissions
- [[Per capita emissions]]

Research & organisations
 - [[Case studies]]
 - [[DIMPACT]]
 - [BBC White Paper, Sept 2020](https://downloads.bbc.co.uk/rd/pubs/whp/whp-pdf-files/WHP372.pdf)
 - [Carbon Trust & DIMPACT's Carbon impact of video streaming](https://www.carbontrust.com/resources/carbon-impact-of-video-streaming)

Initatives
 - [[Sustainable filmmaking initatives]]
 - [[Green hosting]]
 - [[Crypto Climate Accord]]
 - [[Verified Carbon Units]]
 - [[Verified Carbon Units]]
 - [[Web page optimisation]]
 - [Open Climate](https://branch.climateaction.tech/issues/issue-2/open-climate-now/)
 - [Greening of Streaming](https://www.greeningofstreaming.org)


## Video's share of online emissions

> "My estimate for video streaming is 0,19g of CO2 per hour, while the Shift Project calculated that [video streaming emits 400g of CO2 per hour](https://theshiftproject.org/article/shift-project-vraiment-surestime-empreinte-carbone-video-analyse/). That's about 1,000 more!" François Zaninotto [ref](https://marmelab.com/blog/2020/09/21/web-developer-climate-change.html)
 
In other words, estimates vary:

- Audio and video takes up 63% of web's footprint: https://www.climatecare.org/resources/news/infographic-carbon-footprint-internet/
- [[Shift Project]] – 400g/hour of video (https://theshiftproject.org/wp-content/uploads/2020/06/2020-06_Did-TSP-overestimate-the-carbon-footprint-of-online-video_EN.pdf)
- 80% of the web's bandwidth is used for video; while the web and devices accessing it make up an estimate 3.7% of global emissions ([The Shift Project](https://theshiftproject.org/wp-content/uploads/2019/03/Lean-ICT-Report_The-Shift-Project_2019.pdf)), more than airlines.
- G Kamiya, IEA – 36g/hour of video  / 0.077kwh https://www.iea.org/commentaries/the-carbon-footprint-of-streaming-video-fact-checking-the-headlines
- [[DIMPACT]] – 88-93g CO2e – viewing over streaming, cable and satellite platforms used a mean of 0.17–0.18 KWh per device-hour (88–93 gCO2e) while terrestrial broadcast used a mean of 0.07 kWh (36 gCO2e). https://www.sciencedirect.com/science/article/pii/S0195925521001116?via%3Dihub
- Carbon Trust & Netflix - 55g CO2e / hour in Europe - https://prod-drupal-files.storage.googleapis.com/documents/resource/public/Carbon-impact-of-video-streaming.pdf. Dimpact used the same methodology to put worldwide CO2e at [under 100g](https://about.netflix.com/en/news/the-true-climate-impact-of-streaming).


## How is it calculated?
There are many different calculations for the CO2 intensity per MB of video shared online raning by a factor of up to 100. [Why do they differ so much](https://getpocket.com/read/3093145118)? [[Digital CO2 analysis]] is hard to calculate because it depends on many factors: 
- the CPU intensity in generating and serving the media; 
- where the content is served from, what [[CDNs]] is being used.
- where the content is served to; 
- the delivery network it (may) use;
- the device it is loaded on (a significant percentage of the footprint depends on whether it's streaming to smart phone, flat-screen TV or something in between); 
- the transfer protocol (1gb of streaming uses less energy that 1gb downloaded). 

And that's without considering 
- the *embedded footprint* in the production of the media. There are a number of media industry [[Sustainable filmmaking initatives]] looking at this
- the *embedded footprint* of all the technology from server, through the network to delivery. 
- the *behavioural impact footprint* of the media's message (e.g. a one minute burger commercial vs a 90m minute climate-change focussed documentary; Top Gear vs a David Attenborough). Sustainable certifiers [Albert](wearealbert.or), now require (tho don't monitor) [an 'editorial content' question]( https://wearealbert.org/2020/11/25/alberts-new-editorial-question-explained/) during certification.
- the *compulsiveness* of the media – 60 hour series stack or short film?


 