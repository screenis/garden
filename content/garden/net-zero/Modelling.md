# Modelling

### Three main schools of thought for Digital ITC footprint:

According to researcher [Gautheir Roussilhe](http://gauthierroussilhe.com) there are three main schools of thought for modelling digital ICT climate emissions:
- [Jens Malmodin](https://www.datacenterdynamics.com/en/profile/jens-malmodin/) - works at Ericsson, Sweden)
- [Anders S. G. Andrae](https://www.researchgate.net/profile/Anders-Andrae) - works at Huawei, Sweden. His model with Tomas Edler was [used by The Shift Project](https://theshiftproject.org/en/article/unsustainable-use-online-video/) (see below) 
- [Lofti Belkhir](https://www.eng.mcmaster.ca/people/faculty/lotfi-belkhir) - works at McMasters University, US. Author of [Assessing ICT global emissions footprint: Trends to 2040 & recommendations](https://www.researchgate.net/publication/322205565_Assessing_ICT_global_emissions_footprint_Trends_to_2040_recommendations) - "if unchecked, ICT GHGE relative contribution could grow from roughly 1–1.6% in 2007 to exceed 14% of the 2016-level worldwide GHGE by 2040". 

Malmodin & Andrae are both Swedish; Malmodin is consider an optimist; Andrae & Lofti are considered pessimistic. [Roussilhe sets this out on page 31 of this French language analysis](http://gauthierroussilhe.com/pdf/NTE-Mars2021.pdf) that each view the ratio of impacts between Data Centre, Networks and Devices differently:


|   %                      |   Andrae      |   Belkhir      |   Malmodin    |
|-------------------------|---------------|----------------|---------------|
|   Network               |   35          |   22           |   24          |
|   Devices               |   33          |   37           |   57          |
|   Data centres          |   33          |   41           |   18          |
|   Total footprint size  |   623 MtCO2e  |   1207 MtCO2e  |   690 MtCO2e  |

![[Pasted image 20211030133503.png]]

### The Shift Project
**The [[Shift Project]]**, Paris, March 2019 - 3.7% of total emissions in 2019, growing from 2.5% in 2013 ([source](https://theshiftproject.org/wp-content/uploads/2019/03/Lean-ICT-Report_The-Shift-Project_2019.pdf)). Widely quoted (the [BBC](https://www.bbc.com/future/article/20200305-why-your-internet-habits-are-not-as-clean-as-you-think), [Climate Care](https://www.climatecare.org/resources/news/infographic-carbon-footprint-internet/) & the [New Scientist](https://www.newscientist.com/article/2209569-streaming-online-pornography-produces-as-much-co2-as-belgium/)). Scope include production. Shift blames the growth on video.

Shift's estimates for video streaming have been challenged by the IEA: https://www.iea.org/commentaries/the-carbon-footprint-of-streaming-video-fact-checking-the-headlines - which [Shift responded to](https://theshiftproject.org/en/article/shift-project-really-overestimate-carbon-footprint-video-analysis/), acknowledging the error but not adjusting their basic figure.