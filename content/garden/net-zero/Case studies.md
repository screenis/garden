# Case studies

### UK - BBC comparison of media

The [BBC compared streaming](https://downloads.bbc.co.uk/rd/pubs/whp/whp-pdf-files/WHP372.pdf) against broadcast, satellite and cable:
-  iPlayer at 0.19 kWh/device-hour [98 gCO2e/device-hour], 
-  satellite at 0.16 kWh/device-hour [82 gCO2e/device-hour], 
-  cable at 0.15 kWh/device-hour [78 gCO2e/device-hour], 
-  terrestrial at 0.06 kWh/device-hour [31 gCO2e/device-hour].

### France - detailed anatomy of one Canal+ Video
Researched by [GreenSpector](https://greenspector.com), did [a case study comparing](https://greenspector.com/en/impact-playing-canal-video/) the impact of playing a Canal + video depending on its bandwidth, and the device streamed to, with a range from SD on a smartphone, to TV on UHD – and a difference in impact of 80x. They recognised the following variables:
 - Visualization - looking at SD, HD and UHD
 - Device energy consumption - smartphone, tablet, laptop, PC, TV-cable, TV satellite, TV ethernet
 - Network costs, varying between fixed wire and GSM mobile:
	 - 13 Wh / Go for the core network (source CONVINcE)
	 - 30 to 40 Wh / Go for the fixed network
	 - 150 Wh / Go for the GSM network (source CONVINcE)
 - CDN costs - they found examples from 0.04wh to 1wh and settled on 0.14 Wh / h.
 - Encoding/decoding, 
	 - For IPTV: 0.09 Wh for one hour of video
	 - For the OTT: 0.30 Wh for one hour of video
 They settled on "across the **Canal+ fleet, the average end-to-end consumption is 214 Wh per hour of video**". France has a very clean grid as it's almost entirely nuclear powered, so **the impact ranges from 20 to 66 g CO2 eq** (IEA estimate for france was 8g - under 25% of their global 36g figure. Interestingly, the variance wasn't so great between output type once everything was added together:
 
 ![](https://greenspector.com/wp-content/uploads/2020/12/Impact-of-playing-an-hour-of-video-min.png)
 
 **In this same study, the estimate of the purchase of a DVD is 400g EqCO2,** which therefore **leads to the impact of watching a 2-hour video. 5 times less than that of buying a DVD** in France and equivalent in the US.

### Global - music video worked example
Mostly tho the data is missing so estimates must make guesses. An estimate was made for the 7 billion online views of the music video Despacito https://www.linkedin.com/pulse/despacito-streaming-energy-consumption-rabih-bashroush/ and came to **908.93 GWh** (energy, not hardware).

However, Gauthier Roussilhe says:
>  I've reviewed the Linkedin article about Despacito. The calculation seems way off because he used a high-end estimate of energy use for data transmission. According to Coroama the consensus for networks will be around 0,01 kWh/hour up to 0,06 kWh in 2015. The author is using a massive 1,6 kWh/hour."

---

> "I wrote a library to generate random data called [Faker](https://github.com/fzaninotto/Faker). As of today, it's been [installed 116 Million times](https://packagist.org/packages/fzaninotto/faker) and probably executed a hundred times more. If each execution of Faker emits a milligram of CO2, **I'm now responsible for 11,6 Metric tons of CO2 emissions**. Had I designed Faker to be just a bit more energy efficient in the first place, the effects would already be huge today."  François Zaninotto [ref](https://marmelab.com/blog/2020/09/21/web-developer-climate-change.html)

If the Internet were a country, it would be [the sixth largest](https://www.oreilly.com/library/view/designing-for-sustainability/9781491935767/) in terms of electricity use. ICT doesn’t just produce [between 2.1% and 3.9% of global greenhouse gas (GHG) emissions](https://www.sciencedirect.com/science/article/pii/S2666389921001884) – more than aviation or shipping – but as more of the world get online at faster speeds, watching more video for longer, this share is rising fast.