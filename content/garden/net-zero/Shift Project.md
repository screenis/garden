# Shift Project
The [Shift Project](https://theshiftproject.org) estimates 35% of electrity consumed with the web comes from devices; the remaining 65% comes from data centers, network setups and servers. A further 45% of emissions come from device manufacturing ([Manufacturing an iPhone7 emits 56kg of CO2 equivalent](https://particuliers.ademe.fr/conso/conso-responsable/acheter-un-smartphone-reconditionne-quels-avantages)). They break down the emissions [as follows](https://theshiftproject.org/wp-content/uploads/2019/03/Executive-Summary_Lean-ICT-Report_EN_lowdef.pdf):

-   45% for production
-   20% for terminals usage (desktop, laptop, mobile, TV, console)
-   19% for data centers usage
-   16% for networks usage