 # Lifecycle analysis

> “In France, we tend to follow a Life-Cycle Assessment (LCA) approach that includes four factors: GHG emissions (CO2eq), water consumption (Liters), abiotic resources consumption (Sbeq. or Antimony equivalent) and energy consumption (MJ). Like everybody else we measure these impacts on three different poles: data centers, networks and end-user equipment… So what’s the difference with the English-speaking approach? The main focus seems to be on reducing carbon emissions through the vector of electricity.”
	[Gauthier Roussilhe](http://gauthierroussilhe.com/post/digital-sustainability-french.html)

## Water, Resources, Energy & CO2eqv (wrec)

### Water
In the rush to cut CO2 emissions, giant data centre owners like Google have switched from energy-intensive air conditioning to cool the servers, for water cooling. With data centres often built in hot and dry regions, the demand for billions of gallons of water in drought-prone regions creates a new and growing problem.

> “In Red Oak, Texas, a town about 20 miles south of Dallas, Google wants as much as 1.46 billion gallons of water a year for a new data center… Ellis County, which includes Red Oak and roughly 20 other towns, will need almost 15 billion gallons for everything from irrigation to residential use.”
https://time.com/5814276/google-data-centers-water/

### Resources
>**before you start using a piece of electronic equipment, it has already emitted 70% of its lifetime CO2**. So the best you can do to avoid emissions is to [avoid buying or renewing an electronic device](https://particuliers.ademe.fr/parents/ado/garder-son-smartphone-le-plus-longtemps-possible) in the first place."
	https://marmelab.com/blog/2020/09/21/web-developer-climate-change.html
	
- 70 kilos of resources to make a smart phone: https://meta.eeb.org/2018/03/15/calls-grow-to-stop-making-smartphones-that-are-designed-to-break/
- e-waste makes up 70% of the toxic waste in US landfills: https://www.weforum.org/agenda/2018/02/how-do-we-tackle-the-fastest-growing-waste-stream-on-the-planet/
- 56 kg CO2 eq CO2 to make an Apple iPhone 7 against 9kg CO2eqv for a refurbished phone. https://44bxqv2nhtiz6xc5b29j46tw-wpengine.netdna-ssl.com/wp-content/uploads/Rapport-RSE-Recommerce.pdf
- [Manufacturing a laptop emits 200kg of CO2 equivalent](http://seeds4green.net/sites/default/files/es303012r.pdf). Som manufacturers publish life-cycle assessments of – eg [these by Dell](https://www.dell.com/learn/us/en/uscorp1/corp-comm/environment_carbon_footprint_products)).
- [a rack server emits 380kg of CO2 equivalent](http://seeds4green.net/sites/default/files/es303012r.pdf)

### Energy
"For example, a 50-inch LED television consumes much more electricity than a smartphone (100 times) or laptop (5 times). Because phones are extremely energy efficient, data transmission accounts for more than 80% of the electricity consumption when streaming.."
https://www.iea.org/commentaries/the-carbon-footprint-of-streaming-video-fact-checking-the-headlines