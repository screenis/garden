# Web page optimisation
### Improving YouTube
https://www.wired.co.uk/article/youtube-digital-waste-interaction-design

- YouTube's annual carbon footprint in 2019 was about 10Mt CO2e - about the output of a city the size of Glasgow.
- "eliminating the "digital waste" of showing video images to users who are only listening to audio — halting that could slash YouTube's carbon footprint by up to 500kt CO2e each year, on par with about 30,000 homes"

### Smaller pages
- [[Tiny css]] frameworks and methods.