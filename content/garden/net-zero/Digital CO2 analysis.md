# Digital CO2 analysis

## Estimates of total size
- According to researcher [Gautheir Roussilhe](http://gauthierroussilhe.com) there are three main schools of thought for [[Modelling]] ICT climate emissions. In addition:
- **Boston Consulting Group**, 2021 - refers to 3-4% of global emissions https://www.bcg.com/publications/2021/building-sustainable-telecommunications-companies. This was up from 2% of global emissions, 1 billion tonnes in 2018. https://www.sciencefocus.com/science/what-is-the-carbon-footprint-of-the-internet/.
- **Meta review** by Tom Greenwood - "Reviewing the academic literature on the energy consumption of internet data, we found that figures varied from the lowest of 0.004 kWh/GB to 136 kWh/GB" https://www.wholegraindigital.com/blog/website-energy-consumption/ - and settled on 1.8 kWh/GB

## It's only growing
-  "if unchecked, ICT GHGE relative contribution could grow from roughly 1–1.6% in 2007 to exceed 14% of the 2016-level worldwide GHGE by 2040" https://www.sciencedirect.com/science/article/abs/pii/S095965261733233X?via%3Dihub
-  "BCG predicting that data centers alone will use 8% of global electricity by 2030." https://www.bcg.com/publications/2021/building-sustainable-telecommunications-companies	- 

## What is the unit of measurement?
Many favour the focus on KwH per GB as the CO2eqv will vary based on location for instance:
> "In France, where I live, producing 1 kilowatt.hour of electricity emits on average [55g of CO2 equivalent](https://www.rte-france.com/eco2mix/les-emissions-de-co2-par-kwh-produit-en-france#) (it's much less than most other countries, e.g. Germany's electricity emits [440g eqCO2/kWh](https://www.eea.europa.eu/data-and-maps/daviz/co2-emission-intensity-5#tab-googlechartid_chart_11_filters=%7B%22rowFilters%22%3A%7B%7D%3B%22columnFilters%22%3A%7B%22pre_config_ugeo%22%3A%5B%22European%20Union%20(current%20composition)%22%3B%22France%22%3B%22Germany%22%5D%7D%7D), and the US [450gCO2/kwh](https://www.eia.gov/tools/faqs/faq.php?id=74&t=11), but in return, we have nuclear waste buried underground for milleniums to come)." https://marmelab.com/blog/2020/09/21/web-developer-climate-change.html

## What is 100g of CO2?
A great tool for finding real-world equivalencies to emissions quantities is https://www.epa.gov/energy/greenhouse-gas-equivalencies-calculator.

From https://marmelab.com/blog/2020/09/21/web-developer-climate-change.html: 
-    Breathing alone emits about [1kg CO2 per day](https://www.sciencefocus.com/planet-earth/how-much-does-human-breathing-contribute-to-climate-change/)
-   A car trip emits [1.9kg CO2 for 10km](https://particuliers.ademe.fr/au-bureau/deplacements/calculer-les-emissions-de-carbone-de-vos-trajets)
-   Producing a t-shirt emits [5 kgs of CO2](https://www.ademe.fr/sites/default/files/assets/documents/acv-biens-equipements-201809-synthese.pdf), and a tennis racket [23kg of CO2](https://www.ademe.fr/sites/default/files/assets/documents/acv-biens-equipements-201809-synthese.pdf)
-   A 150g beef steak emits [5.4kg of CO2 equivalent](https://www.bilans-ges.ademe.fr/documentation/UPLOAD_DOC_FR/index.htm?repas.htm)
-   A red rose bought for Valentine's day emits more than [350g of CO2](https://www.youtube.com/watch?v=0XQWkA_eeDk)
-   

## Some measurement tools
The first four all follow the 'enter your URL', get a readout method:
- **Website Carbon Calculator**: https://www.websitecarbon.com/ - uses the Green Web Foundation's [[Green hosting]] directory API, and Anders Andrae and Tomas Edler 2017 [paper](https://www.mdpi.com/2078-1547/6/1/117#abstract) to create a network estimate of 1.8kWh/GB. Also avaiable as a script/widget to display CO2 per page.
-** Ecograder**: https://ecograder.com/ - uses the Green Web Foundation's [[Green hosting]] directory API
- **Ecometer**: http://ecometer.org/ - looks at design practices (http requests, plugins, css compression, fonts, image resizing, etc)
- **GreenFrame**: https://greenframe.io
- **Microsoft Sustainability Caluclator**: https://appsource.microsoft.com/en-us/product/power-bi/coi-sustainability.sustainability_dashboard
- **Power API**: https://powerapi-ng.github.io/index.html
- **EcoIndex** - http://ecoindex.fr/

France seems quite developed in this space; as well as GreenFrame, there's https://greenspector.com/en/home/, http://www.ecoindex.fr/, http://ecometer.org/, https://theshiftproject.org, [GreenFrame.io](https://greenframe.io/) & Marmelab. A great explanation of the difference in the French approach is described by Gauthier Roussilhe [here](http://gauthierroussilhe.com/post/digital-sustainability-french.html).

## Example
DEFRA - ICT emission reduction analysis
https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/997523/ict-annual-report-2019-20.pdf
 

## Some Models
### GreenFrame.io
https://greenframe.io/ | developers include [Pierre-Etienne Moreau](https://twitter.com/pemoreau), [François Zaninotto](https://github.com/fzaninotto) | contact@greenframe.io
 
 One of the more developed models, by the French developers MarmeLab, it distinguishes between three CO2 origins: CPU, Network and Screen. So a [[Static Site Generators]] might have a lot less carbon in its delivery onliny, but the CPU build might be offset the gain if the build happens frequently (MarmeLab do a good comparison [of the two here](https://marmelab.com/blog/2021/03/04/argos-comparing-the-energy-consumption-of-two-web-stacks.html)). [More info](https://marmelab.com/blog/2021/04/08/greenframe-io-website-carbon.html). 
 
![Wh Repartition](https://marmelab.com/static/8c2bcdc3d7eb8a1e46014600cd606dfc/ec911/server-network-device-wh.png "Wh Repartition")

An advantage of GreenFrame is it can help you design systems that minimise footprint in the areas outside of control (screen, network to an extent) and focus on what can be controlled – CPU/server end. In the analysis above they found the screen was responsible for 72% of the energy in some processes, concluding "the importance of screen consumption (in a desktop client) validates efforts to improve the performance of web apps: the longer it takes for a user to achieve a task, the more energy they consume".

### On Global Electricity Usage of Communication Technology: Trends to 2030
by Anders Andrae and Tomas Edler
https://www.mdpi.com/2078-1547/6/1/117#abstract

As reported in [Nature](https://www.nature.com/articles/d41586-018-06610-y), and used by Website Carbon Calculator - to give 1.8kwh/gb.

## Examples

Analysis for visuali.st:
- Ecometer - http://ecometer.org/report?url=https%3A%2F%2Fvisuali.st%2F
- Greenframe - https://greenframe.io/results/visuali-st
- Website Carbon Calculator - https://www.websitecarbon.com/website/visuali-st/

