# Global Carbon Index
The [IHS Markit priced](https://carboncreditcapital.com/value-of-carbon-market-update-2021/) one [[Verified Carbon Units]] at $34.99/tonne in May 2021

"The IHS Markit Global Carbon Index tracks the most liquid segment of the tradable carbon credit futures markets. Constituents of the Global Carbon Index include futures contracts on European Union Allowances (EUA), California Carbon Allowances (CCA) and the Regional Greenhouse Gas Initiative (RGGI), with pricing data from [OPIS by IHS Markit](https://www.opisnet.com/) Pricing (North American Pricing) and ICE Futures Pricing (European Pricing)."
https://ihsmarkit.com/products/global-carbon-index.html

IHS Markit is a registry, including the Markit Metaregistry, launched with the Global Carbon Council, Gold Standard, UK Woodland Carbon Code, UK Peatland Code, and Verra.

Offsetting markets include:
 - https://www.carbonfootprint.com/offset.aspx?o=10