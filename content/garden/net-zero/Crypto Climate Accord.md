# Greenchain

## Crypto Climate Accord

"seeks to transition all blockchains to renewable energy by 2030 or sooner. It sets a 2040 target for the crypto industry to reach “net zero” emissions, which would involve reducing pollution and turning to strategies that might be able to suck the industry’s historical carbon dioxide emissions out of the atmosphere."
from: https://www.theverge.com/2021/4/8/22373524/cryptocurrency-climate-accord-bitcoin-carbon-footprint

150 organisations funding 'renewably powered crypto', including Ripple and Consensys

https://cryptoclimate.org/



## RedGrid
https://redgrid.io/

