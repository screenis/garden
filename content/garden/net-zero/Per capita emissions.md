# Per capita emissions / CO2

If trying to estimate the emissions of a worker, this can be based on the per-capita emissions which range from an average 
- 8.4 tonnes/year in the EU (https://ec.europa.eu/eurostat/databrowser/view/t2020_rd300/default/table?lang=en),
- 8.1 tonnes /year in China (https://knoema.com/atlas/China/CO2-emissions-per-capita)
- to over 15.2 tonnnes in the US (https://data.worldbank.org/indicator/EN.ATM.CO2E.PC?locations=US).
