# Hello!

This is a space to explore research questions…

- what is [[Open Video]], now and imagined?

- What is [[Net-zero online video]] and how to achieve it?

- what [[Tools]] are useful?

[[double brackets]] links are internal and open to the right of this pane; other links are external and open a new page in a new window/tab.

## About this garden

This is a work-in-progress [digital garden](https://maggieappleton.com/garden-history/) by [nicol](https://github.com/vingle), writing in [Obsidian](https://obsidian.md/) and publishing via [Gitlab](https://gitlab.com/screenis/garden/-/tree/master/content/garden). Please feel free to contribute / comment / etc on Gitlab or via nicol at screen.is.

The design is based on [Andy's (lovely) Working Notes](https://notes.andymatuschak.org/About_these_notes), which saves you from having to command-click internal links, as they are demarked in [[double brackets]] , instead loading them in columns. It's running on [Gatsby](https://www.gatsbyjs.com/), a [[Static Site Generators]], in a [theme](https://github.com/mathieudutour/gatsby-digital-garden/) by [Mathieu](https://mathieu.dutour.me/).

Any original content is [CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/).